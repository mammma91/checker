﻿using LiteDB;
using Newtonsoft.Json;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;


namespace nextCheck.Classes
{
    public class BrandViewList
    {
        MainWindow main;
        WrapPanel viewList;
        public  List<BrandView> brandList = new List<BrandView>();
        public BrandViewList(MainWindow mainWindow)
        {
            main = mainWindow;
            viewList = mainWindow.xUpdateChecks;
        }

        public BrandView getBrandView(Brand brand)
        {
            try
            {
                foreach (BrandView brandView in brandList)
                {
                    if (brandView.brand.name.Equals(brand.name, StringComparison.OrdinalIgnoreCase))
                    {
                        return brandView;
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine("GetBrandView: "+ex.ToString());
            }

            return null;
        }

        public void addBrandToView()
        {
            Brand newBrand = new Brand();
            AddBrandWindow brandForm = new AddBrandWindow(newBrand);
            if (brandForm.ShowDialog() == true)
            {
                using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                {
                    var col = db.GetCollection<DataBase.Brand>("brands");
                    newBrand = col.FindOne(x => x.name.Equals(newBrand.name));
                }
                bool exist = false;
                foreach (BrandView b in viewList.Children)
                {
                    if (b.brand.name.Equals(newBrand.name))
                    {
                        exist = true;
                        break;
                    }
                }
                if (!exist)
                {
                    BrandView v = new BrandView(newBrand, main);
                    v.Style = main.FindResource("styleBrandView") as Style;
                    v.FocusVisualStyle = main.FindResource("focusBrandView") as Style;
                    v.Click += brandViewBtn_Click;
                    viewList.Children.Add(v);                  
                    updateBrandList();
                    updateCountSku(v);
                    checkBrandsFromServer();
                    update();
                    sort();
                    main.addLog("Brand added: " + newBrand.name);
                }
                else
                {
                    main.addLog("Brand already exist: " + newBrand.name);
                }
            }
        }

        public void addBrandsToView()
        {
            viewList.Children.Clear();

            List<Brand> listBrand = null;
            //using (ApplicationContext db = new ApplicationContext())
            //{
            //    listBrand = db.Brands.ToList();                
            //}
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
            {
                listBrand = db.GetCollection<DataBase.Brand>("brands").FindAll().ToList();
            }
            foreach (Brand br in listBrand)
            {
                if (br.isChecking)
                {
                    BrandView v = new BrandView(br, main);
                    v.Style = main.FindResource("styleBrandView") as Style;
                    v.FocusVisualStyle = main.FindResource("focusBrandView") as Style;
                    v.Click += brandViewBtn_Click;
                    viewList.Children.Add(v);
                }
            }
            
            updateBrandList();
            updateCountSku();
            checkBrandsFromServer();
            update();
            sort();          

        }

        public void updateCountSku(BrandView brandView)
        {
            int count = 0;
            string name = MainWindow.selectName;
            Console.WriteLine("------------------------------: ");
            Console.WriteLine("------------------------------: ");
            DateTime t = DateTime.Now;
            try
            {
                //using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "importers.db"))
                //{
                //    var colSheet = dbsheet.GetCollection<Importer>("Importer");
                //    colSheet.EnsureIndex(x => x.select);
                //    name = colSheet.FindOne(x => x.select == true).name;
                //}
                using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "allMissing.db"))
                {
                    var colSheet = dbsheet.GetCollection<Row>("Missing");
                    colSheet.EnsureIndex(x => x.brand);
                    List<Row> rows = colSheet.Find(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (rows.Count == 0) throw new Exception();
                    Console.WriteLine("Count of rows: " + rows.Count);
                    Row header = rows.Find(x => x.sku.Equals("HEADER"));
                    Console.WriteLine("Hedear count: " + header.cells.Count);
                    int h = header.cells.IndexOf("'importername");
                    foreach (string s in header.cells)
                    {
                        Console.Write(s + "|");
                    }
                    Console.WriteLine();
                    Console.WriteLine("index: " + h);
                    if (h >= 0)
                    {
                        foreach (Row r in rows)
                        {
                            if (name.Equals("ALL") || (r.cells[h] != null && r.cells[h].Contains(name)))
                                count++;
                        }
                        Console.WriteLine("Count of my skus: " + count);
                    }
                }
            }
            catch { }
            main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (count == 0)
                    brandView.labelCount.Content = "";
                else
                    brandView.labelCount.Content = count.ToString();
            });
            DateTime t2 = DateTime.Now;
            TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
            Console.WriteLine("Get sheet Time: " + (elapsedSpan.TotalSeconds));
        }

        public void updateCountSku()
        {
            string name = MainWindow.selectName;
            Console.WriteLine("------------------------------: ");
            DateTime t = DateTime.Now;
            using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "allMissing.db"))
            {
                var colSheet = dbsheet.GetCollection<Row>("Missing");               
                colSheet.EnsureIndex(x => x.brand);
                foreach (BrandView brandView in brandList)
                {
                    int count = 0;
                    List<Row> rows = colSheet.Find(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)).ToList();
                    if (rows.Count == 0) {
                        main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                        {
                            brandView.labelCount.Content = "";
                        });
                        continue;
                    }
                    Row header = rows.Find(x => x.sku.Equals("HEADER"));
                    int h = header.cells.IndexOf("'importername");
                    if (h >= 0)
                    {
                        foreach (Row r in rows)
                        {
                            if (name.Equals("ALL") || (r.cells[h] != null && r.cells[h].Contains(name)))
                                count++;
                        }
                    }
                    main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    {
                        //if (count == 0)
                        //    brandView.labelCount.Foreground = new SolidColorBrush() { Color = Colors.Silver };
                        //else
                        //    brandView.labelCount.Foreground = new SolidColorBrush() { Color = Colors.Orange };
                        if (count == 0)
                            brandView.labelCount.Content = "";
                        else
                            brandView.labelCount.Content = count.ToString();

                    });
                }
            }

            DateTime t2 = DateTime.Now;
            TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
            Console.WriteLine("Get sheet Time: " + (elapsedSpan.TotalSeconds));
        }


        public void updateBrandList()
        {
            brandList.Clear();
            foreach (BrandView br in viewList.Children)
            {
                brandList.Add(br);
            }
        }
        //public void updateBrandList(BrandView bV)
        //{
        //    brandList.Clear();
        //    brandList.Add(bV);
        //}

        private void brandViewBtn_Click(object sender, RoutedEventArgs e)
        {
            BrandView brandView = sender as BrandView;
            if (brandView.isChecked) brandView.check(false);
            else brandView.check(true);
            update();
        }

        public void checkAll()
        {
            foreach (BrandView br in viewList.Children)
            {
                br.check(true);
            }
            update();
        }

        public void checkAllEmpty()
        {
            foreach (BrandView br in viewList.Children)
            {
                if (br.brand.time == 0)
                    br.check(true);
                else
                    br.check(false);
            }
            update();
        }

        public void checkAllRotten()
        {
            foreach (BrandView br in viewList.Children)
            {
                if (br.rotten)
                    br.check(true);
                else
                    br.check(false);
            }
            update();
        }

        public void deleteFromView()
        {
            List<BrandView> list = new List<BrandView>();
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
            {
                var col = db.GetCollection<DataBase.Brand>("brands");
                foreach (BrandView br in viewList.Children)
                {
                    if (br.isChecked)
                    {
                        br.brand.isChecking = false;

                        //db.Brands.Update(br.brand);
                        col.Update(br.brand);
                        list.Add(br);
                    }
                }
                //db.SaveChanges();
            }
            foreach (BrandView br in list)
            {
                viewList.Children.Remove(br);
                brandList.Remove(br);
            }
            updateBrandList();
            update();
            sort();
        }

        public void selectByDate()
        {
            foreach (BrandView br in viewList.Children)
            {
                long date1 = 0;
                long date2 = DateTime.Now.Ticks;
                if (main.datePicker1.SelectedDate != null) date1 = main.datePicker1.SelectedDate.Value.Ticks;
                if (main.datePicker2.SelectedDate != null) date2 = main.datePicker2.SelectedDate.Value.Ticks;

                if (date1 < br.brand.timeCreate && date2 > br.brand.timeCreate)
                    br.check(true);
                else
                    br.check(false);
            }
            update();           
        }

        
        public void selectByError()
        {
            foreach (BrandView br in viewList.Children)
            {
                if (br.isError())
                    br.check(true);
                else
                    br.check(false);
            }
            update();
        }

        public void selectByRotten(int min)
        {
            foreach (BrandView br in viewList.Children)
            {
                long now = DateTime.Now.Ticks;
                double res = TimeSpan.FromTicks(now - br.brand.timeLastUpdate).TotalMinutes;
                if (res>min)
                    br.check(true);
                else
                    br.check(false);
            }
            update();
        }

        public void checkBrandsFromServer()
        {
            try
            {
                if (MainWindow.checkServer && MainWindow.client.getStatus())
                {
                    List<Brand> brands = new List<Brand>();
                    for (int i = 0; i < brandList.Count; i++)
                    {
                        brands.Add(brandList[i].brand);
                    }
                    List<Brand> answer = MainWindow.client.sendBrandsInfo(brands);
                    using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                    {
                        var col = db.GetCollection<DataBase.Brand>("brands");
                        foreach (Brand b in answer)
                        {
                            Brand brand = col.FindOne((x => x.name.Equals(b.name)));
                            if ((brand.folderID == null || brand.folderID.Equals("")) && (b.folderID != null && !b.folderID.Equals("")))
                            {
                                BrandView bv = getBrandView(brand);
                                bv.brand.folderID = b.folderID;
                                main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                                {
                                    bv.check(brand.isChecking);
                                });
                                brand.folderID = b.folderID;
                                col.Update(brand);
                            }
                            else
                            {
                                int compareTime = brand.time.CompareTo(b.time);
                                if (compareTime < 0 && brand.time!=0)
                                {
                                    main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                                    {
                                        BrandView bv = getBrandView(brand);
                                        if (bv!=null)
                                            bv.changeLabelHasNew(true);
                                        else
                                            Console.WriteLine("NULL: brand view - "+brand.name);
                                    });
                                }
                            }
                        }
                    }

                    //for (int i=0;i<brandList.Count;i++)
                    //{
                    //    BrandView br = brandList[i];
                    //    if (br.isChecked && (br.brand.folderID == null || br.brand.folderID.Equals("")))
                    //    {
                    //        main.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                    //                (ThreadStart)delegate ()
                    //                {
                    //                    br.animateLoading();
                    //                }
                    //        );
                    //        string answer = MainWindow.client.sendBrandInfo(br.brand);
                    //        if (!answer.Equals("next"))
                    //        {
                    //            br.brand.folderID = answer;
                    //            br.brand.error = "";
                    //            checkStatus("error", br);
                    //        }
                    //        else
                    //        {
                    //            br.brand.folderID = "";
                    //            br.brand.dateTime = "";
                    //            br.brand.time = 0;
                    //            br.brand.timeCreate = 0;
                    //            br.brand.checkName = "";
                    //            br.brand.checkID = "";
                    //            br.brand.link = "";
                    //            br.brand.owner = "";
                    //            br.brand.jobber = "";
                    //            br.brand.application = "";
                    //            br.brand.error = "";
                    //            checkStatus("error", br);
                    //        }
                    //        br.isLoading = false;
                    //    }
                    //}
                    //main.isLoading = false;
                    //main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                    //{
                    //    update();
                    //    sort();
                    //});
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Search Brands for server: " + ex);
            }
        }

        public void search()
        {
            foreach (BrandView br in brandList)
            {
                if (MainWindow.isSearching)
                {
                    if (br.isChecked)
                    {
                        if((br.brand.checkID==null || br.brand.checkID.Equals("") )&& (br.brand.link != null && !br.brand.link.Equals("")))
                        {
                            br.brand.checkID = br.brand.link.Substring(br.brand.link.IndexOf(@"id=") + 3, 33);
                            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
                            {
                                var col = db.GetCollection<DataBase.Brand>("brands");
                                col.Update(br.brand);
                            }
                        }
                        main.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                br.animateLoading();
                                if(br.rotten)
                                    br.changeLabelHasNew(false);
                            }
                         );
                        string status = MainWindow.googleDrive.searchCheck(br.brand);
                        checkStatus(status, br);
                        if (MainWindow.checkServer && MainWindow.client != null && MainWindow.client.getStatus())
                        {

                            string answer = MainWindow.client.sendBrandInfo(br.brand);
                            if (!answer.Equals("next"))
                            {
                                string oldFolderid = br.brand.folderID;
                                string oldError = br.brand.error;
                                br.brand.folderID = answer;
                                status = MainWindow.googleDrive.searchCheck(br.brand);
                                if (status.Equals("new") || status.Equals("update") || status.Equals("first") || br.brand.folderID == null || br.brand.folderID.Equals(""))
                                {
                                    checkStatus(status, br);

                                }
                                else
                                {
                                    br.brand.folderID = oldFolderid;
                                    br.brand.error = oldError;
                                }

                            }
                        }
                        br.isLoading = false;


                    }
                }
                else
                {
                    break;
                }
            }
            MainWindow.isLoading = false;
            MainWindow.isSearching = false;
            main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate () {
                main.serachBtn.Content = "Искать";
                main.updateResponsibleList();
                update();
                sort();
                main.serachBtn.IsEnabled = true;
            });
            
            MessageBox.Show("Поиск Завершен");
        }

        public void search(BrandView br)
        {
            if (MainWindow.isSearching)
            {
                if (br.isChecked)
                {
                    if ((br.brand.checkID == null || br.brand.checkID.Equals("")) && (br.brand.link != null && !br.brand.link.Equals("")))
                    {
                        br.brand.checkID = br.brand.link.Substring(br.brand.link.IndexOf(@"id=") + 3, 33);
                        using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
                        {
                            var col = db.GetCollection<DataBase.Brand>("brands");
                            col.Update(br.brand);
                        }
                    }
                    main.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                        (ThreadStart)delegate ()
                        {
                            br.animateLoading();
                            if (br.rotten)
                                br.changeLabelHasNew(false);
                        }
                     );
                    string status = MainWindow.googleDrive.searchCheck(br.brand);
                    checkStatus(status, br);
                    if (MainWindow.checkServer && MainWindow.client != null && MainWindow.client.getStatus())
                    {

                        string answer = MainWindow.client.sendBrandInfo(br.brand);
                        if (!answer.Equals("next"))
                        {
                            string oldFolderid = br.brand.folderID;
                            string oldError = br.brand.error;
                            br.brand.folderID = answer;
                            status = MainWindow.googleDrive.searchCheck(br.brand);
                            if (status.Equals("new") || status.Equals("update") || status.Equals("first") || br.brand.folderID == null || br.brand.folderID.Equals(""))
                            {
                                checkStatus(status, br);

                            }
                            else
                            {
                                br.brand.folderID = oldFolderid;
                                br.brand.error = oldError;
                            }

                        }
                    }
                    br.isLoading = false;
                }
            }

            MainWindow.isLoading = false;
            MainWindow.isSearching = false;
            main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate () {
                main.updateResponsibleList();
                main.serachBtn.Content = "Искать";
                update();
                sort();
                main.serachBtn.IsEnabled = true;
            });
            MessageBox.Show("Поиск Завершен");
        }

        public void checkStatus(string status, BrandView br)
        {
            Console.WriteLine("Status: "+status);

            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
            {
                if (!status.Equals("error"))
                {
                    br.brand.timeLastUpdate = DateTime.Now.Ticks;
                    Console.WriteLine("lastTime: " + br.brand.timeLastUpdate);
                }
                var col = db.GetCollection<Brand>("brands");
                col.Update(br.brand);
                //db.Update(br.brand);
                //db.SaveChanges();
            }
            updateCountSku(br);

            main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                if (status.Equals("new"))
                {
                    br.labelNew.Content = "New";
                    br.labelNew.Visibility = System.Windows.Visibility.Visible;
                }
                if (status.Equals("update"))
                {
                    br.labelNew.Content = "Update";
                    br.labelNew.Visibility = System.Windows.Visibility.Visible;
                }
            });
            
        }

        public void searchInView(object sender)
        {
            int i = 0;
            TextBox textBox = (TextBox)sender;
            if (textBox.Text != "")
            {
                int length = viewList.Children.Count;
                for(int j=0;j< length;j++)
                {
                    BrandView br = (BrandView)viewList.Children[j];
                    if (br.brand.name.StartsWith(textBox.Text, StringComparison.OrdinalIgnoreCase))
                    {
                        br.check(true);
                        br.Visibility = Visibility.Visible;
                        viewList.Children.Remove(br);
                        viewList.Children.Insert(i, br);
                        i++;
                    }
                    else
                    {
                        br.check(false);
                        br.Visibility = Visibility.Hidden;
                    }
                }
                i = 0;
                main.scrollViewer.ScrollToTop();
            }
            else
            {
                foreach (BrandView br in viewList.Children)
                {
                    br.check(true);
                    br.Visibility = Visibility.Visible;
                }
                sort();
            }
            update();
        }

        public void sort()
        {
            switch (main.sortBox.SelectedIndex)
            {
                case 1:
                    {
                        List<BrandView> list = new List<BrandView>();
                        BrandView[] l = new BrandView[viewList.Children.Count];
                        viewList.Children.CopyTo(l, 0);
                        list.AddRange(l);
                        list.Sort(delegate (BrandView br1, BrandView br2)
                        { return br1.brand.name.CompareTo(br2.brand.name); });
                        viewList.Children.Clear();
                        foreach (BrandView br in list)
                        {
                            if (!br.isError())
                                viewList.Children.Add(br);
                            else
                            {
                                viewList.Children.Insert(0, br);
                            }
                        }
                        break;
                    };
                case 0:
                    {
                        List<BrandView> list = new List<BrandView>();
                        BrandView[] l = new BrandView[viewList.Children.Count];
                        viewList.Children.CopyTo(l, 0);
                        list.AddRange(l);
                        list.Sort(delegate (BrandView br1, BrandView br2)
                        { return br2.brand.timeCreate.CompareTo(br1.brand.timeCreate); });
                        viewList.Children.Clear();
                        foreach (BrandView br in list)
                        {
                            if (!br.isError())
                                viewList.Children.Add(br);
                            else
                            {
                                viewList.Children.Insert(0, br);
                            }
                        }
                        break;
                    };
            }                  
        }

        public void uncheckALL()
        {
            foreach (BrandView br in viewList.Children)
            {
                br.check(false);
            }
            update();
        }

        public void update()
        {
            main.textTotalBrands.Text = viewList.Children.Count.ToString();
            int errorsCount = 0;
            int checkedCount = 0;
            foreach (BrandView br in viewList.Children)
            {
                if (br.isError()) errorsCount++;
                if (br.isChecked) checkedCount++;
            }
            main.textErrors.Text = errorsCount.ToString();
            main.textCountCheck.Text = checkedCount.ToString();
        }

        public void Notify(string value)
        {
            Brand newBrand = JsonConvert.DeserializeObject<Brand>(value);

            BrandView brandView = getBrandView(newBrand);
            if (brandView != null)
            {
                main.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                {
                    brandView.changeLabelHasNew(true);
                    //MainWindow.notifier.ShowInformation("Новый Чек: "+ newBrand.name);
                });            
            }
        }


    }
}
