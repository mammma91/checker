﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util;
using Google.Apis.Util.Store;
using LiteDB;
using Newtonsoft.Json;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Threading;

namespace nextCheck.Classes
{
    public class GoogleDrive
    {
        DriveService service;
        public MainWindow mainWindow;
        List<BrandView> list;
        public string user = "";
        public string mail = "";
        public string token = "";

        public GoogleDrive(MainWindow mainWindow, List<BrandView> list)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            this.mainWindow = mainWindow;
            this.list = list;
        }
        public GoogleDrive(MainWindow mainWindow)
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
            this.mainWindow = mainWindow;
        }
        public GoogleDrive()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");
        }

        public void dispose()
        {
            service.Dispose();
        }

        public bool AuthenticateOauth()
        {
            string userName = "user";
            string clientSecretJson = "";
            if (System.IO.File.Exists(Directory.GetParent(Environment.CurrentDirectory) + @"\key\client_secret_268529130243-8bav00a2nnvlc5m4gaa1eccpftjlkf5e.apps.googleusercontent.com.json"))
            {
                clientSecretJson = Directory.GetParent(Environment.CurrentDirectory) + @"\key\client_secret_268529130243-8bav00a2nnvlc5m4gaa1eccpftjlkf5e.apps.googleusercontent.com.json";
            }
            else
            {
                clientSecretJson = Environment.CurrentDirectory + @"\files\key\client_secret_268529130243-8bav00a2nnvlc5m4gaa1eccpftjlkf5e.apps.googleusercontent.com.json";
            }
            try
            {
                if (string.IsNullOrEmpty(userName))
                    throw new ArgumentNullException("userName");
                if (string.IsNullOrEmpty(clientSecretJson))
                    throw new ArgumentNullException("clientSecretJson");
                if (!System.IO.File.Exists(clientSecretJson))
                    throw new Exception("clientSecretJson file does not exist.");

                string[] scopes = new string[] { DriveService.Scope.DriveReadonly };                                            
                UserCredential credential;
                string credPath;
                using (var stream = new FileStream(clientSecretJson, FileMode.Open, FileAccess.Read))
                {
                    credPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    credPath = Path.Combine(credPath, ".credentials/", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(GoogleClientSecrets.Load(stream).Secrets,
                                                                             scopes,
                                                                             userName,
                                                                             CancellationToken.None,
                                                                             new FileDataStore(credPath, true)).Result;
                    
                }
                service = new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Drive Oauth2 Authentication Sample"
                });
                //token = credential.Token.AccessToken;
                //Console.WriteLine("Google: Token - " + token);

                token = credential.GetAccessTokenForRequestAsync().Result;
                Console.WriteLine("Google: Token - " + token);
            }
            catch (Exception ex)
            {
                Console.WriteLine("CreateServiceAccountDriveFailed - " + ex.ToString());
                return false;
            }

            try
            {
                AboutResource.GetRequest oRequest = service.About.Get();
                oRequest.Fields = "*";
                About oResponse = oRequest.Execute();
                user = oResponse.User.DisplayName;
                mail = oResponse.User.EmailAddress;

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Google API: " + e.InnerException);
            }
            return false;
        }

        public void DownloadFile(string id,string path)
        {
            try
            {
                var request = service.Files.Get(id);
                var stream = new System.IO.MemoryStream();

                request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
                {
                    switch (progress.Status)
                    {
                        case Google.Apis.Download.DownloadStatus.Completed:
                            {
                                Console.WriteLine("Download complete.");
                                SaveStream(stream, path);
                                break;
                            }
                        case Google.Apis.Download.DownloadStatus.Failed:
                            {
                                Console.WriteLine("Download failed.");
                                throw new Exception("Download failed.");
                            }
                    }
                };
                request.Download(stream);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        private static void SaveStream(System.IO.MemoryStream stream, string saveTo)
        {
            try
            {
                using (System.IO.FileStream file = new System.IO.FileStream(saveTo, System.IO.FileMode.Create, System.IO.FileAccess.Write))
                {
                    stream.WriteTo(file);
                }
            }catch(Exception ex)
            {
                throw ex;
            }
        }

        public void findCheck(Brand brand)
        {
            if (brand.folderID == null || brand.folderID.Equals(""))
            {
                throw new Exception("Error: empty FolderID!");
            }
            brand.error = "";
            brand.jobber = "";
            brand.application = "";
            Google.Apis.Drive.v3.Data.File file = null;
            List<Google.Apis.Drive.v3.Data.File> list = null;
            if ((list = getManyChecks(brand.folderID)) != null)
            {
                if (list.Count > 1)
                {
                    Console.WriteLine("Сортировка файлов");
                    list.Sort(delegate (Google.Apis.Drive.v3.Data.File f1, Google.Apis.Drive.v3.Data.File f2)
                    { return f2.CreatedTime.Value.Ticks.CompareTo(f1.CreatedTime.Value.Ticks); });
                }
                //foreach (Google.Apis.Drive.v3.Data.File f in list)
                //{
                //    Console.WriteLine(f.ModifiedTime.Value.Ticks.ToString());
                //    Console.WriteLine(f.MimeType);
                //}
                string cutName = brand.name;
                if (cutName.Contains(" ")) cutName = cutName.Substring(0, cutName.IndexOf(" "));
                bool isCutName = false;
                foreach (Google.Apis.Drive.v3.Data.File f in list)
                {
                        if (f.Name.IndexOf(cutName, StringComparison.OrdinalIgnoreCase) >= 0 && f.Name.IndexOf("check)", StringComparison.OrdinalIgnoreCase) >= 0 && f.Name.IndexOf("~", StringComparison.OrdinalIgnoreCase) == -1)
                        {
                            isCutName = true;
                            file = f;
                        Console.WriteLine("Взят первый файл по имени");
                        break;
                        }
                }
                if (!isCutName && list.Count > 0)
                {
                    foreach (Google.Apis.Drive.v3.Data.File f in list)
                    {
                        if (f.Name.IndexOf("check)", StringComparison.OrdinalIgnoreCase) >= 0 && f.Name.IndexOf("~", StringComparison.OrdinalIgnoreCase) == -1)
                        {
                            isCutName = true;
                            file = f;
                            Console.WriteLine("Взят первый файл");
                            break;
                        }
                    }
                    if(file==null && list.Count == 1 && list[0].Name.IndexOf(".txt", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        file = list[0];
                        Console.WriteLine("Взят единственный файл txt");
                    }
                }
                if (file!=null)
                {                   
                    if (file.Name.IndexOf(".txt", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        Brand fileTxt = new Brand();
                        fileTxt.set(brand);
                        fileTxt.checkID = file.Id;
                        fileTxt.checkName = file.Name;
                        fileTxt.time = file.ModifiedTime.Value.Ticks;
                        fileTxt.error = "";
                        getIdFromTxt(fileTxt);
                        if (fileTxt.error.Equals(""))
                        {
                            brand.folderID = fileTxt.folderID;
                            findCheck(brand);
                        }
                        else
                        {
                            throw new Exception("Error: not found FolderID in TXT");
                        }
                    }
                    else
                    {
                        brand.checkID = file.Id;
                        brand.checkName = file.Name;
                        brand.dateTime = file.CreatedTime.Value.ToShortDateString();
                        brand.timeCreate = file.CreatedTime.Value.Ticks;
                        brand.time = file.ModifiedTime.Value.Ticks;
                        brand.link = "https://drive.google.com/open?id=" + file.Id;
                        brand.owner = file.Owners[0].DisplayName;
                    } 
                }
                else
                {
                    Console.WriteLine("Don't found checks: " + brand.name);
                    brand.error = "Don't found checks: " + brand.name;
                }
            }
            else
            {
                Console.WriteLine("Empty folder: " + brand.name);
                brand.error = "Empty folder: " + brand.name;
            }
        }

        public string searchCheck(Brand brand) {
            string answer = "error";
            try
            {
                if (brand.checkName!=null && !brand.checkName.Equals("") && !System.IO.File.Exists(MainWindow.pathChecks + brand.checkName))
                {
                    Console.WriteLine("Повторное скачивание чека");
                    DownloadFile(brand.checkID, MainWindow.pathChecks + brand.checkName);
                }
            }
            catch
            {
                Console.WriteLine("Failed to download file - old");
            }
            Brand file = new Brand();
            file.set(brand);
            try
            {
                List<Google.Apis.Drive.v3.Data.File> filesList = getManyFiles(brand.folderID);
                if (filesList.Count == 0)
                {
                    filesList.Add(getFile(brand.folderID));
                    Console.WriteLine("В папке нету папок");
                }
                else
                if (filesList.Count > 1)
                {
                    Console.WriteLine("Сортировка папок");

                    filesList.Sort(delegate (Google.Apis.Drive.v3.Data.File f1, Google.Apis.Drive.v3.Data.File f2)
                    { return f2.CreatedTime.Value.CompareTo(f1.CreatedTime.Value); });
                }else
                if (filesList.Count == 1 && filesList[0].Name.Equals("old", StringComparison.OrdinalIgnoreCase))
                {
                    throw new Exception("Папка содержит только OLD");
                }

                foreach (Google.Apis.Drive.v3.Data.File f in filesList)
                {
                    if (f.Name.IndexOf(brand.name, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        filesList.Insert(0, f);
                        Console.WriteLine("Взята первая папка по имени бренда");
                        break;
                    }
                }

                foreach (Google.Apis.Drive.v3.Data.File folder in filesList)
                {
                    if(!folder.Name.Equals("old", StringComparison.OrdinalIgnoreCase))
                    {
                        file.folderID = folder.Id;
                        findCheck(file);
                        if (!file.error.Equals("")) {
                            brand.error = file.error;
                            continue; 
                        }
                        Console.WriteLine("Drive File: " + file.checkName);
                        Console.WriteLine("Brand File: " + brand.checkName);
                        Console.WriteLine("Drive File: " + file.checkID);
                        Console.WriteLine("Brand File: " + brand.checkID);
                        Console.WriteLine("Drive time: " + file.time);
                        Console.WriteLine("Brand time: " + brand.time);
                        if (file.checkName.Equals(brand.checkName) && file.time.CompareTo(brand.time) ==0)
                        {
                            brand.error = "";
                            return "same";
                        }
                        if (file.time.CompareTo(brand.time) <0)
                        {
                            brand.error = "";
                            return "old";
                        }
                        answer = "new";

                        if(brand.checkID != null && file.checkID.Equals(brand.checkID))
                        {
                            
                            if (brand.checkName != null && file.checkName.Equals(brand.checkName))
                            {
                                //-----CLEAR UPDATE
                                answer = "update";
                            }
                            else
                            {
                                //-----DIRTY UPDATE
                                file.timeCreate = file.time;
                                DateTime t = new DateTime(file.timeCreate);
                                file.dateTime = t.ToShortDateString();
                                Console.WriteLine("Чек тот же, но с другим именем");
                            }
                            
                        }
                        //-----Try to download file
                        string pathFile = MainWindow.pathChecks + file.checkName;
                        if (System.IO.File.Exists(pathFile))
                        {
                            if (answer.Equals("update"))
                            {
                                System.IO.File.Delete(pathFile);
                                Console.WriteLine("Скачка файла - апдейт");
                                DownloadFile(file.checkID, pathFile);
                            }
                            else
                            {
                                Console.WriteLine("Файл уже есть");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Скачка файла");
                            DownloadFile(file.checkID, pathFile);
                        }

                        //{
                        //    //-----Try to download file
                        //    string pathFile = MainWindow.pathChecks + file.checkName;
                        //    if (System.IO.File.Exists(pathFile))
                        //    {
                        //        if (file.time.CompareTo(brand.time) > 0 && brand.checkName != null && file.checkName.Equals(brand.checkName))
                        //        {
                        //            System.IO.File.Delete(pathFile);
                        //            Console.WriteLine("Скачка файла - апдейт");
                        //            DownloadFile(file.checkID, pathFile);
                        //            answer = "update";
                        //        }
                        //        else
                        //        {
                        //            Console.WriteLine("Файл уже есть");
                        //            //mainWindow.addLog("The following file: \"" + file.checkName + "\" already exists."); 
                        //        }
                        //    }
                        //    else
                        //    {
                        //        if (brand.checkID != null && file.checkID.Equals(brand.checkID)) //--------&& file.timeCreate.CompareTo(brand.timeCreate) == 0 ------------------NEED CHECK!!!!
                        //        {
                        //            file.timeCreate = file.time;
                        //            DateTime t = new DateTime(file.timeCreate);
                        //            file.dateTime = t.ToShortDateString();
                        //            Console.WriteLine("Чек тот же, но с другим именем");
                        //            answer = "update";
                        //        }
                        //        Console.WriteLine("Скачка файла");
                        //        DownloadFile(file.checkID, pathFile);
                        //    }
                        //}
                        Check check = new Check(file);
                        Console.WriteLine("Работа с чеком");
                        check.getValuesFromCheck();
                        if (!file.error.Equals("")) throw new Exception("Error with new check.");
                        if (!check.compareName() && file.nameInCheck.Equals("") && MainWindow.autoConfirm)
                        {
                            Console.WriteLine("Сверка имени");
                            mainWindow.openMatchBrandsWindow(file, check.brandsInCheck);
                        }
                        if (!file.error.Equals(""))
                        {
                            if (System.IO.File.Exists(MainWindow.pathChecks + file.checkName))
                                System.IO.File.Delete(MainWindow.pathChecks + file.checkName);
                            answer = "error";
                            Console.WriteLine("Ошибка после сверки имен чека");
                            brand.error = "Ошибка после сверки имен бренда";
                            continue;
                        }
                        else 
                        {
                            //using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
                            //{
                            //    foreach (Sheet sheet in check.sheets)
                            //    {
                            //        var colSheet = db.GetCollection<Row>(sheet.name.Replace(" ", ""));
                            //        Console.WriteLine(sheet.name);
                            //        colSheet.EnsureIndex(x => x.brand);
                            //        colSheet.DeleteMany(x => x.brand.Equals(file.nameInCheck, StringComparison.OrdinalIgnoreCase));
                            //        colSheet.InsertBulk(sheet.getRows(file.nameInCheck));
                            //    }
                            //}
                            foreach (Sheet sheet in check.sheets)
                            {
                                using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + sheet.name.Replace(" ", "") + ".db"))
                                {
                                    var colSheet = dbsheet.GetCollection<Row>("sheet");
                                    Console.WriteLine(sheet.name);
                                    colSheet.EnsureIndex(x => x.brand);
                                    colSheet.DeleteMany(x => x.brand.Equals(file.nameInCheck, StringComparison.OrdinalIgnoreCase));
                                    colSheet.Insert(new Row() {sku="HEADER", brand=file.nameInCheck, brandNameFromFile=file.name, cells=sheet.headers });
                                    
                                }
                                if (sheet.name.Equals("SKUs missing on CARiD", StringComparison.OrdinalIgnoreCase))
                                {
                                    using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "allMissing.db"))
                                    {
                                        var colSheet = dbsheet.GetCollection<Row>("Missing");
                                        Console.WriteLine("Add to DB missing sku: " + sheet.name);
                                        colSheet.EnsureIndex(x => x.brand);
                                        colSheet.DeleteMany(x => x.brand.Equals(file.nameInCheck, StringComparison.OrdinalIgnoreCase));
                                        colSheet.InsertBulk(sheet.getRows(file.nameInCheck));
                                    }
                                }
                            }
                            brand.error = "";
                        }
                        //------Delete old check
                        if (answer.Equals("new") && brand.checkName !=null && !brand.checkName.Equals(""))
                        {
                            Console.WriteLine("Удаляем старый чек");
                            bool toDelete = true;
                            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
                            {
                                foreach (Brand b in db.GetCollection<DataBase.Brand>("brands").FindAll().ToList())
                                {
                                    if (!b.name.Equals(brand.name) && b.checkName != null && b.checkName.Equals(file.checkName))
                                    {
                                        toDelete = false;
                                        break;
                                    }
                                }
                            }
                            if (System.IO.File.Exists(MainWindow.pathChecks + brand.checkName) && toDelete)
                                System.IO.File.Delete(MainWindow.pathChecks + brand.checkName);
                        }
                        else if(brand.checkName == null || brand.checkName.Equals(""))
                        {
                            answer = "first";
                        }
                        //------Update Brand
                        using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))//using (ApplicationContext db = new ApplicationContext())
                        {
                            var col = db.GetCollection<DataBase.Brand>("brands");
                            Console.WriteLine("обновляем бренд");
                            string fID = brand.folderID;
                            brand.set(file);
                            Console.WriteLine("Brand Time: "+ brand.time);
                            Console.WriteLine("Brand Time Create: "+ brand.timeCreate);
                            brand.folderID = fID;
                            col.Update(brand);
                            //db.Update(brand);
                            //db.SaveChanges();
                        }
                        
                        return answer;
                    }
                }  
                                
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка в поиске");
                Console.WriteLine(ex.ToString());
                //mainWindow.addLog("Failed : " + brand.name);
                //mainWindow.addLog(ex.ToString());
                brand.error = ex.ToString();
                answer = "error";
            }
            return answer;
        }

        public void sortFiles(List<Google.Apis.Drive.v3.Data.File> filesList)
        {
            Google.Apis.Drive.v3.Data.File temp;
            Console.WriteLine("Pre ");
            for (int i = 0; i < filesList.Count - 1; i++)
            {
                for (int j = 0; j < filesList.Count - i - 1; j++)
                {
                    if (filesList[j + 1].ModifiedTime.Value.Ticks.CompareTo(filesList[j].ModifiedTime.Value.Ticks) < 0)
                    {
                        Console.WriteLine("in "+ j);
                        temp = filesList[j + 1];
                        filesList[j + 1] = filesList[j];
                        filesList[j] = temp;
                    }
                }
            }
            Console.WriteLine("After sort");
            foreach (Google.Apis.Drive.v3.Data.File f in filesList)
            {
                Console.WriteLine(f.ModifiedTime.Value.Ticks.ToString());
            }
        }

        public void getIdFromTxt(Brand file)
        {
            Console.WriteLine("Работа с текстовым файлом");
            string pathTxt = MainWindow.pathChecks + file.checkName;
            if (!System.IO.File.Exists(pathTxt))
            {
                DownloadFile(file.checkID, pathTxt);
            }
            try
            {
                string[] lines = System.IO.File.ReadAllLines(pathTxt);
                if (System.IO.File.Exists(pathTxt))
                    System.IO.File.Delete(pathTxt);
                foreach (string line in lines)
                {
                    if (line.IndexOf(@"open?id=", StringComparison.OrdinalIgnoreCase) >= 0 || line.IndexOf(@"/folders/", StringComparison.OrdinalIgnoreCase) >= 0 || line.IndexOf(@"/file/d/", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        string id = "";
                        if (line.IndexOf(@"open?id=", StringComparison.OrdinalIgnoreCase) >= 0)
                            id = line.Substring(line.IndexOf(@"id=") + 3, 33);
                        else if (line.IndexOf(@"/folders/", StringComparison.OrdinalIgnoreCase) >= 0)
                            id = line.Substring(line.IndexOf(@"/folders/") + 9, 33);
                        else
                            id = line.Substring(line.IndexOf(@"/file/d/") + 8, 33);

                        Google.Apis.Drive.v3.Data.File f = getFile(id);
                        if (!f.MimeType.Equals("application/vnd.google-apps.folder") && f.Name.IndexOf("check)", StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            file.folderID = f.Parents[0];
                            return;
                        }
                        if (f.MimeType.Equals("application/vnd.google-apps.folder"))
                        {
                            file.folderID = f.Id;
                            return;
                        }
                    }
                }
                file.error = "Not found in txt";
                Console.WriteLine("Ошибка в текстовом1");
            }
            catch (Exception ex)
            {
                file.error = "Not found in txt";
                Console.WriteLine("Ошибка в текстовом2");
                Console.WriteLine(ex.ToString());
            }            
        }


        public Google.Apis.Drive.v3.Data.File getFile(string id)
        {
            Google.Apis.Drive.v3.Data.File file = new Google.Apis.Drive.v3.Data.File();
            file.Id = id;
            var request = service.Files.Get(file.Id);
            request.Fields = "id, name,parents,mimeType";

            try
            {
                return request.Execute();

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Google.Apis.Drive.v3.Data.FileList getFiles(string id)
        {

            var request = service.Files.List();
            request.Q = "'" + id + "' in parents";
            request.Fields = "files(id, name,parents,mimeType,createdTime,owners)";

            try
            {
                return request.Execute();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Google.Apis.Drive.v3.Data.File> getFolderChecks(string id)
        {

            List<Google.Apis.Drive.v3.Data.File> result = new List<Google.Apis.Drive.v3.Data.File>();

            string pageToken = null;
            do
            {
                try
                {
                    FilesResource.ListRequest request = service.Files.List();
                    request.PageSize = 100;
                    request.OrderBy = "createdTime desc";
                    request.Q = "'" + id + "' in parents and name contains 'checked' and trashed = false and mimeType = 'application/vnd.google-apps.folder'";
                    request.Fields = "nextPageToken, files(id, name, parents, mimeType, trashed, createdTime)";
                    request.PageToken = pageToken;
                    FileList files = request.Execute();
                    result.AddRange(files.Files);
                    pageToken = files.NextPageToken;
                    Console.WriteLine("Iteration of pageToken: " + pageToken);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                    pageToken = null;
                }
                
            } while (pageToken != null);
            return result;

        }

        public List<Google.Apis.Drive.v3.Data.File> getManyFiles(string id)
        {
            List<Google.Apis.Drive.v3.Data.File> result = new List<Google.Apis.Drive.v3.Data.File>();

            string pageToken = null;
            do
            {
                try
                {
                    FilesResource.ListRequest request = service.Files.List();
                    request.PageSize = 1000;
                    request.OrderBy = "createdTime desc";
                    request.Q = "'" + id + "' in parents and  trashed = false and mimeType = 'application/vnd.google-apps.folder'";
                    request.Fields = "nextPageToken, files(id, name, parents, mimeType, trashed, createdTime)";
                    request.PageToken = pageToken;
                    FileList files = request.Execute();
                    result.AddRange(files.Files);
                    pageToken = files.NextPageToken;
                    Console.WriteLine("Iteration of pageToken: " + pageToken);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                    pageToken = null;
                }
              
            } while (pageToken != null);
            return result;

        }

        public List<Google.Apis.Drive.v3.Data.File> getManyChecks(string id)
        {
            List<Google.Apis.Drive.v3.Data.File> result = new List<Google.Apis.Drive.v3.Data.File>();

            string pageToken = null;
            do
            {
                try
                {
                    FilesResource.ListRequest request = service.Files.List();
                    request.PageSize = 1000;
                    request.OrderBy = "createdTime desc";
                    request.Q = "'" + id + "' in parents and trashed = false and mimeType != 'application/vnd.google-apps.folder'";
                    request.Fields = "nextPageToken, files(id, name, parents, mimeType, trashed, createdTime, owners, modifiedTime)";
                    request.PageToken = pageToken;
                    FileList files = request.Execute();
                    result.AddRange(files.Files);
                    pageToken = files.NextPageToken;
                    Console.WriteLine("Iteration of pageToken: " + pageToken);
                }
                catch (Exception e)
                {
                    Console.WriteLine("An error occurred: " + e.Message);
                    pageToken = null;
                }

            } while (pageToken != null);
            return result;

        }

    }
}

/*
 *
  public void searchParentFromFile()
        {
            GoogleDrive gd = new GoogleDrive(this);
            gd.AuthenticateOauth();
            addLog("start searching");
            using (ExcelBook book = new ExcelBook())
            {
                book.openFile(pathFiles+"brands.xlsx", false);
                object[,] arr = book.getAllValues("Brands");
                int sizeRow = book.lastCell.Row;
                if (sizeRow != null && sizeRow>1)
                {
                    addLog("получен список из корневой всего - " + sizeRow);
                    for (int i = 2; i < sizeRow; i++)
                    {
                        try
                        {
                            Google.Apis.Drive.v3.Data.File file = null;
                            if ((file = gd.getFile(arr[i, 2].ToString())) != null)
                            {
                                book.worksheet.Cells[i, 3] = file.Name;
                                book.worksheet.Cells[i, 4] = "https://drive.google.com/open?id=" + file.Id.ToString();
                                book.worksheet.Cells[i, 5] = "https://drive.google.com/drive/folders/" + file.Id.ToString();
                                Console.WriteLine("Process:" + (i-1));
                            }
                        }
                        catch
                        {
                            book.worksheet.Cells[i, 3] = "Error";
                        }

                        //List<Google.Apis.Drive.v3.Data.File> listChecked = null;
                        //if ((listChecked = gd.getFolderChecks(arr[i,2].ToString())) != null && listChecked.Count > 0)
                        //{
                        //    book.worksheet.Cells[i , 3] = listChecked[0].Name;
                        //    book.worksheet.Cells[i , 4] = listChecked[0].Id.ToString();
                        //    addLogo("обработано - " + (i - 1));
                        //}


                    }
                    addLogo("Done");
                }
                book.saveFile();
            }
            gd.dispose();
        }


        public void searchParent()
        {
            GoogleDrive gd = new GoogleDrive(this);
            gd.AuthenticateOauth();
            string id = "0B8JYM-a6sNVNazRuNTFWdFNhWjQ";
            addLogo("start searching");
            List<Google.Apis.Drive.v3.Data.File> list = null;
            using (ExcelBook book = new ExcelBook())
            {
                book.createFile();
                book.addSheet("Brands");
                book.setSheet("Brands");
                book.worksheet.Cells[1, 1] = "BrandFolderName";
                book.worksheet.Cells[1, 2] = "BrandFolderID";
                book.worksheet.Cells[1, 3] = "FolderName";
                book.worksheet.Cells[1, 4] = "FolderID";
                if ((list = gd.getManyFiles(id)) != null)
                {
                    addLogo("получен список из корневой всего - " + list.Count);
                    for (int i = 0; i < list.Count; i++)
                    {
                        book.worksheet.Cells[i + 2, 1] = list[i].Name;
                        book.worksheet.Cells[i + 2, 2] = list[i].Id.ToString();
                        if (list[i].Trashed == false && list[i].MimeType.Equals("application/vnd.google-apps.folder"))
                        {
                            List<Google.Apis.Drive.v3.Data.File> listChecked = null;
                            if ((listChecked = gd.getFolderChecks(list[i].Id.ToString())) != null && listChecked.Count > 0)
                            {
                                book.worksheet.Cells[i + 2, 3] = listChecked[0].Name;
                                book.worksheet.Cells[i + 2, 4] = listChecked[0].Id.ToString();
                                addLogo("обработано - " + (i + 1));
                            }
                        }
                        
                    }
                }
                book.saveFile(MainWindow.pathFiles + "brands.xlsx");
            }
            gd.dispose();
        }
 * 
 */
