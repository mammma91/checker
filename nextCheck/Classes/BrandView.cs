﻿using LiteDB;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace nextCheck.Classes
{
    public class BrandView : Button
    {
        public Brand brand { get; }
        public bool hasNew = false;
        SolidColorBrush brush, dateBrush;
        TextBlock text = new TextBlock();
        TextBlock text2 = new TextBlock();
        MenuItem menuOpen, menuDelete, menuSearchCheck, menuDeleteYes, openUrlFolder;
        public Label labelNew, labelCount;
        Thread searchThread;
        public ColorAnimation loadAnimation;
        public bool isChecked { get; set; }
        public bool isLoading { get; set; }
        public bool rotten = false;
        
        MainWindow mainWindow;
       
        public BrandView(Brand brand, MainWindow mainWindow)
        {
            this.brand = brand;
            if (brand.nameInCheck == null) brand.nameInCheck = "";
            this.mainWindow = mainWindow;
            brush = new SolidColorBrush();
            

            //text = new TextBlock();
            text.IsEnabled = true;
            text.Text = brand.name;
            text.Foreground = new SolidColorBrush() { Color = Colors.White};
            text.Background = new SolidColorBrush();
            text.Background.Opacity = 0d;
            
            text.TextWrapping = System.Windows.TextWrapping.Wrap;
            text.TextAlignment = System.Windows.TextAlignment.Center;
            //text.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);
            //text2 = new TextBlock();
            //text2.IsEnabled = false;
            text2.Text = brand.dateTime;
            text2.FontSize = 14;
            dateBrush = new SolidColorBrush() { Color = Colors.LightSkyBlue };
            text2.Foreground = dateBrush;
            text2.Background = new SolidColorBrush();
            text2.Background.Opacity = 0d;
            text2.TextWrapping = System.Windows.TextWrapping.Wrap;
            text2.TextAlignment = System.Windows.TextAlignment.Center;
            //text2.BorderThickness = new System.Windows.Thickness(0, 0, 0, 0);
            text2.Margin = new System.Windows.Thickness(0, 0, 0, 10);

            DockPanel dockPanel = new DockPanel();
            DockPanel dockPanelTop = new DockPanel();
            dockPanelTop.Width = 100;
            dockPanelTop.Height = 25;
            dockPanelTop.Margin = new System.Windows.Thickness(0, 0, 0, 0);
            dockPanelTop.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            dockPanel.Width = 100;
            dockPanel.Height = 100;
            dockPanel.Margin = new System.Windows.Thickness(0, 0, 0, 0);
            dockPanel.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            labelNew = new Label();
            labelNew.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            labelNew.FontSize = 12;
            labelNew.Visibility = System.Windows.Visibility.Hidden;
            labelNew.Content = "NEW";
            labelNew.Foreground = new SolidColorBrush() { Color = Colors.White };
            labelNew.Background = new SolidColorBrush() { Color = Colors.Green, Opacity =0.5};
            DockPanel.SetDock(labelNew, Dock.Left);

            labelCount = new Label();
            labelCount.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;
            labelCount.Margin = new System.Windows.Thickness(0, 0, 4, 0);
            labelCount.FontSize = 12;
            labelCount.Content = 0;
            labelCount.FontWeight = FontWeights.Bold;
            labelCount.Foreground = new SolidColorBrush() { Color = Colors.Orange };
            //labelCount.Background = new SolidColorBrush() { Color = Colors.White, Opacity = 0.5 };
            DockPanel.SetDock(labelCount, Dock.Right);
            dockPanelTop.Children.Add(labelNew);
            dockPanelTop.Children.Add(labelCount);
            DockPanel.SetDock(dockPanelTop, Dock.Top);
            dockPanel.Children.Add(dockPanelTop);
            text2.VerticalAlignment = System.Windows.VerticalAlignment.Bottom;
            DockPanel.SetDock(text2, Dock.Bottom);
            dockPanel.Children.Add(text2);
            text.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            DockPanel.SetDock(text, Dock.Bottom);
            dockPanel.Children.Add(text);
            
            Content = dockPanel;

            Height = 100;
            Width = 100;
          
            Background = brush;
            Margin = new System.Windows.Thickness(2, 2, 2, 2); ;


            loadAnimation = new ColorAnimation();
            loadAnimation.Completed += ButtonAnimation_Completed;
            loadAnimation.Duration = TimeSpan.FromMilliseconds(300);
            loadAnimation.AutoReverse = true;
            loadAnimation.FillBehavior = FillBehavior.Stop;
            isLoading = false;

            ContextMenu menu = new ContextMenu();
            MenuItem menuSetting = new MenuItem();
            menuSetting.Header = "Настройки";
            menuSetting.Click += MenuSetting_Click;
            menuOpen = new MenuItem();
            menuOpen.Header = "Открыть Чек";
            menuOpen.Click += MenuOpen_Click;
            menuDeleteYes = new MenuItem();
            menuDeleteYes.Header = "Да";
            menuDeleteYes.Click += MenuDeleteCheck_Click;
             
            menuDelete = new MenuItem();
            menuDelete.Header = "Удалить";
            openUrlFolder = new MenuItem();
            openUrlFolder.Header = "Открыть Папку";
            openUrlFolder.Click += OpenUrlFolder_Click;
            menuSearchCheck = new MenuItem();
            menuSearchCheck.Header = "Искать";
            menuSearchCheck.Click += MenuSearchCheck_Click;
            menuDelete.Items.Add(menuDeleteYes);;
            menu.Items.Add(menuOpen);
            menu.Items.Add(menuSearchCheck);
            menu.Items.Add(menuDelete);
            menu.Items.Add(openUrlFolder);
            menu.Items.Add(menuSetting);
            menu.Opened += Menu_Opened;

            //Style menuStyle = new Style();
            //menuStyle.Setters.Add(new Setter { Property = Control.BorderThicknessProperty, Value = new Thickness(0) });
            //menuStyle.Setters.Add(new Setter { Property = Control.MarginProperty, Value = new Thickness(0) });
            //menuStyle.Setters.Add(new Setter { Property = Control.PaddingProperty, Value = new Thickness(0) });
            //menuStyle.Setters.Add(new Setter { Property = Control.BackgroundProperty, Value = new SolidColorBrush(Color.FromRgb(24,24,24))});//"#242424"
            //menuStyle.Setters.Add(new Setter { Property = Control.ForegroundProperty, Value = new SolidColorBrush(Colors.White) });
            //menuStyle.Setters.Add(new Setter { Property = Control.BorderBrushProperty, Value = new SolidColorBrush(Colors.Black) });
            //menu.Style = menuStyle;
            //menuDeleteYes.Style = menuStyle;
            //menuOpen.Style = menuStyle;
            //menuDelete.Style = menuStyle;
            //openUrlFolder.Style = menuStyle;
            //menuSearchCheck.Style = menuStyle;
            //menuSetting.Style = menuStyle;
            this.ContextMenu = menu;

            //MouseMove += BrandView_MouseMove;
            //MouseLeave += BrandView_MouseLeave;
            //PreviewMouseDown += BrandView_PreviewMouseDown;

            
            check(true);
        }

        private void OpenUrlFolder_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var url = "https://drive.google.com/open?id=" + brand.folderID;
                Process.Start(url);
            }
            catch (Exception ex)
            {
                Console.WriteLine("HYPERLYNK: "+ex);
            }
        }

        private void MenuSearchCheck_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (!MainWindow.isSearching)
            {
                MainWindow.isSearching = true;
                mainWindow.animateLoading();
                check(true);
                searchThread = new Thread(() => MainWindow.brandViewList.search(this));//new Thread(new ThreadStart(MainWindow.brandViewList.search));
                searchThread.Start();
            }
        }

        private void MenuDeleteCheck_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
            {
                brand.isChecking = false;
                var col = db.GetCollection<DataBase.Brand>("brands");
                Brand b = col.FindOne(x => x.name.Equals(brand.name));
                b.isChecking = false;
                col.Update(b);
                //db.Brands.Update(brand);
                //db.SaveChanges();
            }

            mainWindow.xUpdateChecks.Children.Remove(this);
            MainWindow.brandViewList.update();
        }

        private void Menu_Opened(object sender, System.Windows.RoutedEventArgs e)
        {
            if (brand.checkName == null || brand.checkName.Equals("") || !System.IO.File.Exists(MainWindow.pathChecks + brand.checkName))
                menuOpen.IsEnabled = false;
            else
                menuOpen.IsEnabled = true;
            if (MainWindow.isSearching)
                menuSearchCheck.IsEnabled = false;
            else
                menuSearchCheck.IsEnabled = true;
            if(brand.folderID!=null && !brand.folderID.Equals(""))
            {
                openUrlFolder.IsEnabled = true;
            }
            else
            {
                openUrlFolder.IsEnabled = false;
            }
        }

        private void MenuOpen_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (System.IO.File.Exists(MainWindow.pathChecks + brand.checkName))
            {
                Process myProcess = new Process();
                myProcess.StartInfo.WorkingDirectory = MainWindow.pathChecks;
                myProcess.StartInfo.FileName = MainWindow.pathChecks + brand.checkName;
                myProcess.Start();
            }
        }

        private void MenuSetting_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            BrandSettingWindow brandSettingWindow = new BrandSettingWindow(brand);
            if (brandSettingWindow.ShowDialog() == true)
            {
                check(isChecked);
            }
        }


        public void changeLabelHasNew(bool r)
        {
            rotten = r;
            if (rotten)
            {
                labelNew.Content = "ROTTEN";
                labelNew.Visibility = System.Windows.Visibility.Visible;
                labelNew.Background = new SolidColorBrush() { Color = Colors.OrangeRed, Opacity = 0.4 };
            }
            else
            {
                labelNew.Visibility = System.Windows.Visibility.Hidden;
                labelNew.Content = "NEW";
                labelNew.Background = new SolidColorBrush() { Color = Colors.Green, Opacity = 0.5 };
            }
        }

        public void check(bool ch)
        {
            text.Text = brand.name;
            text2.Text = brand.dateTime;
            if (!isError())
            {
                if (ch)
                {
                    isChecked = ch;
                    brush.Color = Colors.Black;
                    brush.Opacity = 0.6;
                }
                else
                {
                    isChecked = ch;
                    brush.Color = Colors.Black;
                    brush.Opacity = 0.2;
                }
            }
            else
            {
                if (!brand.error.Contains("Папка содержит только OLD"))
                {
                    labelNew.Visibility = System.Windows.Visibility.Hidden;
                    labelNew.Content = "NEW";
                    labelNew.Background = new SolidColorBrush() { Color = Colors.Green, Opacity = 0.5 };
                    if (ch)
                    {
                        isChecked = ch;
                        brush.Color = Colors.Red;
                        brush.Opacity = 0.6;
                    }
                    else
                    {
                        isChecked = ch;
                        brush.Color = Colors.DarkRed;
                        brush.Opacity = 0.2;
                    }
                }
                else
                {

                    labelNew.Content = "OLD";
                    labelNew.Visibility = System.Windows.Visibility.Visible;
                    labelNew.Background = new SolidColorBrush() { Color = Colors.Green, Opacity = 0.3 };
                    if (ch)
                    {
                        isChecked = ch;
                        brush.Color = Colors.DarkOrange;
                        brush.Opacity = 0.6;
                        text.Foreground = new SolidColorBrush() { Color = Colors.White };
                    }
                    else
                    {
                        isChecked = ch;
                        brush.Color = Colors.DarkOrange;
                        brush.Opacity = 0.2;
                        text.Foreground = new SolidColorBrush() { Color = Colors.DarkGray };
                    }
                }

            }

            Background = brush;
            if (!isChecked)
            {
                if (dateBrush != null)
                    dateBrush.Color = Colors.Black;
            }
            else
            {
                if(dateBrush!=null)
                dateBrush.Color = Colors.LightSkyBlue;
            }

        }

        public bool isError()
        {
            if (brand.folderID == null || brand.folderID.Equals("") || brand.name == null || brand.name.Equals("") || (brand.error != null && !brand.error.Equals("")))
            {
                if (brand.error == null) brand.error = "";
                return true;
            }         
            
            return false;
        }


        public void animateLoading()
        {
            isLoading = true;
            loadAnimation.From = Colors.Black;
            loadAnimation.To = Colors.DarkOliveGreen;
            brush.BeginAnimation(SolidColorBrush.ColorProperty, loadAnimation);
        }

        public void ButtonAnimation_Completed(object sender, EventArgs e)
        {
            if (isLoading) { 
                animateLoading();
                
            }
            else
            {
                text2.Text = brand.dateTime;
                check(true);

            }

        }


    }
}
