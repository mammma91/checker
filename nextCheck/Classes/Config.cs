﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nextCheck.Classes
{
    public class Config
    {
        [JsonIgnore]
        public string pathFile = Directory.GetParent(Environment.CurrentDirectory) + @"\config.json";
        public string version { get; set; }
        public string port { get; set; }
        public string ip { get; set; }

        public bool getConfigFromFile()
        {
            try
            {
                Config config = JsonConvert.DeserializeObject<Config>(File.ReadAllText(pathFile));
                version = config.version;
                port = config.port;
                ip = config.ip;
                Console.WriteLine($"Version: {version}  IP: {ip} Port:{port}");
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error: Read config.json", ex);
                return false;
            }
            return true;
        }

        public bool saveConfigToFile()
        {
            try
            {
                File.WriteAllText(pathFile, JsonConvert.SerializeObject(this));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            Console.WriteLine("Saved Config JSON");
            return true;
        }

    }
}
