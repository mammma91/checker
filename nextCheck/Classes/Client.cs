﻿using Google.Apis.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using nextCheck.DataBase;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace nextCheck.Classes
{
    public class Client
    {

        static string userName;
        User user;
        private static string host = "46.33.238.152";//"46.33.238.152";//"192.168.31.78";
        private static string host2 = "10.10.8.100";
        private const int port = 8887;
        static TcpClient client;
        static NetworkStream stream;
        static StreamReader reader;
        static StreamWriter writer;
        public Thread receiveThread;
        Dictionary<string, string> cash = new Dictionary<string, string>();
        public static bool isListening = false;
        public long unixTime = 0;
        static object locker = new object();

        public bool getStatus()
        {
            if (client != null)
                return client.Connected;
            else
                return false;
        }

        public Client(User user)
        {
            this.user = user;
            cash.Add("brand",null);
            cash.Add("get-brands", null);
        }

        public bool start()
        {
            client = new TcpClient();
            try
            {
                client.Connect(host, port);
                stream = client.GetStream();
                reader = new StreamReader(stream);
                writer = new StreamWriter(stream);
                return login();         
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine("Disconnecting");
                Disconnect();
            }
            return false;
        }

        private bool login()
        {
            string userJson = JsonConvert.SerializeObject(user);
            Console.WriteLine("Sending: " + userJson);
            SendMessage(userJson);
            var answer = ReceiveMessage();
            if (answer.Key.Equals("200")) return true;
            else Disconnect();
            return false;
        }

        public void startListening()
        {
            receiveThread = new Thread(new ThreadStart(BroadcastMessage));
            receiveThread.Start();
        }

        private string waitResult(string key)
        {
            string result;
            while (cash[key] == null && isListening)
            {
                Thread.Sleep(20);
            }
            lock (locker)
            {
                result = cash[key];
                cash[key] = null;
            }
            return result;
        }

       
        public string sendBrandInfo(Brand brand)
        {
            string answer = "";
            try
            {
                string json = JsonConvert.SerializeObject(brand);
                SendMessage("brand",json);            
                answer = waitResult("brand"); 
                Brand brandFromClient = JsonConvert.DeserializeObject<Brand>(answer);
                if (!brandFromClient.checkName.Equals("next"))
                {                   
                    answer = brandFromClient.folderID;
                }
                else
                {
                    answer = "next";               
                }
                Console.WriteLine("Answer: " + answer);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при общении с сервером"+ex);
                return "next";
            }
            return answer;
        }

        public List<Brand> sendBrandsInfo(List<Brand> brands)
        {
            try
            {
                string json = JsonConvert.SerializeObject(brands, Formatting.Indented);
                SendMessage("get-brands", json);
                string answer = waitResult("get-brands");
                brands = JsonConvert.DeserializeObject<List<Brand>>(answer);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при общении с сервером" + ex);
            }
            return brands;
        }

        public static void SendMessage(string message)
        {
            try
            {
                writer.WriteLine(message);
                writer.Flush();
            }
            catch
            {
                Console.WriteLine("Подключение прервано!");
                Disconnect();
            }
        }

        public static void SendMessage(string key, string message)
        {
            try
            {

                if (message == null)
                    message = "{\"Key\":\"" + key + "\",\"Value\":\"\"}";
                else
                {
                    message = message.Replace(Environment.NewLine, "");
                    message = message.Trim();
                    message = message.Replace("\"", "\\\"");
                    message = "{\"Key\":\"" + key + "\",\"Value\":\"" + message + "\"}";
                    //message = message.Replace("{", "{\"Key\":\"" + key + "\",");
                }
                Console.WriteLine("Out: " + message);
                writer.WriteLine(message);
                writer.Flush();
            }
            catch
            {
                Console.WriteLine("Подключение прервано!");
                Disconnect();
            }
        }

        public static DictionaryEntry ReceiveMessage()
        {
            string message = "";
            try
            {
                message = reader.ReadLine();
                Console.WriteLine("In: " + message);
                //var key = new { Key = "" };
                //var value = new { Value = "" };
                //var jsonKey = JsonConvert.DeserializeAnonymousType(message, key);
                //var jsonObj = JsonConvert.DeserializeAnonymousType(message, value);
                //if (message.Contains(","))
                //    message = message.Replace("\"Key\":\"" + jsonObj.Key + "\",", "");
                //else
                //    message = "";
                Dictionary<string, string> messageDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(message);
                string v = messageDictionary["Value"].Replace("\\", "");
                return new DictionaryEntry(messageDictionary["Key"], v);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: get message" + ex);
                Disconnect();
                return new DictionaryEntry("error", "");
            }
        }

        //public static string ReceiveMessage()
        //{
        //    string message = "next";
        //    try
        //    {
        //        message = reader.ReadLine();                  
        //        Console.WriteLine(message);
        //    }
        //    catch
        //    {
        //        Console.WriteLine("Подключение прервано!");
        //        Disconnect();
        //    }
        //    return message;
        //}

        public void BroadcastMessage()
        {
            isListening = true;
            DictionaryEntry dictionaryMessage = new DictionaryEntry();
            string key = "";
            string value = "";
            do
            {       
                try
                {
                    dictionaryMessage = ReceiveMessage();
                    key = dictionaryMessage.Key.ToString();
                    value = dictionaryMessage.Value.ToString();
                    if (!key.Equals("error"))
                    {
                        unixTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                        switch (key)
                        {
                            case "key":
                                {
                                    throw new Exception("Close socket");
                                }
                            case "new-version":
                                {
                                    //MainWindow.notifier.ShowWarning("Вышла новая версия, перезапустите программу!");
                                    MessageBox.Show("Вышла новая версия, перезапустите программу!");
                                    break;
                                }
                            case "echo":
                                {
                                    Console.WriteLine("Connection is alive!");
                                    break;
                                }
                            case "new-brand":
                                {
                                    MainWindow.brandViewList.Notify(value);
                                    break;
                                }
                            case "get-brands":
                                {
                                    lock (locker)
                                    {
                                        cash["get-brands"] = value;
                                    }
                                    break;
                                }
                            case "brand":
                                {
                                    lock (locker)
                                    {
                                        cash["brand"] = value;
                                    }
                                    break;
                                }
                        }
                    }
                    else
                    {
                        throw new Exception("Close socket");
                    }
                }
                catch
                {
                    isListening = false;
                    Console.WriteLine("Подключение прервано!"); //соединение было прервано
                    Disconnect();
                    break;
                }
            } while (isListening);
        }

        public bool checkVersion()
        {
            SendMessage("get-version", null);
            var answer = ReceiveMessage();
            if (!answer.Key.Equals("get-version")) return false;
            Config configServer = JsonConvert.DeserializeObject<Config>(answer.Value.ToString());
            Console.WriteLine("Server: -v " + configServer.version);
            Config configClient = new Config();
            if (!configClient.getConfigFromFile())
            {
                return false;
            }
            Console.WriteLine("Client: -v " + configClient.version);

            if (configServer.version.Equals(configClient.version))
            {
                Console.WriteLine("Actual version");
                return false;
            }
            else
            {
                Console.WriteLine("New version");
                SendMessage("get-app",null);
                if (getApp())
                {
                    configServer.saveConfigToFile();
                    return true;
                }
                else
                    return false;
            }
        }

        private bool getApp()
        {
            try
            {
                string cmdFileSize = ReceiveMessage().Value.ToString();
                var key = new { size = "" };
                var jsonKey = JsonConvert.DeserializeAnonymousType(cmdFileSize, key);
                cmdFileSize = jsonKey.size;
                int length = Convert.ToInt32(cmdFileSize);
                if (length == 0) throw new Exception("empty file");
                byte[] buffer = new byte[length];
                int received = 0;
                int read = 0;
                int size = 1024;
                int remaining = 0;
                while (received < length)
                {
                    remaining = length - received;
                    if (remaining < size)
                    {
                        size = remaining;
                    }
                    read = stream.Read(buffer, received, size);
                    received += read;
                }
                using (FileStream fStream = new FileStream(Directory.GetParent(Environment.CurrentDirectory) + @"\app.7z", FileMode.Create))
                {
                    Console.WriteLine("SaveFile IN");
                    fStream.Write(buffer, 0, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: Download app.7z", ex);
                return false;
            }

            return true;
        }

        public static void Disconnect()
        {
            isListening = false;
            if (stream != null)
            {              
                reader.Close();
                writer.Close();
                stream.Close();//отключение потока
                stream = null;
            }
            if (client != null)
            {
                client.Close();//отключение клиента
                client = null;
            }

        }

       public static void StartUpdater()
       {
            Process myProcess = new Process();
            myProcess.StartInfo.WorkingDirectory = Directory.GetParent(Environment.CurrentDirectory) + @"\updater";
            myProcess.StartInfo.FileName = Directory.GetParent(Environment.CurrentDirectory) + @"\updater" + @"\Updater.exe";
            myProcess.Start();
        }

    }
}



/*
 * 
 
        public static string ReceiveMessage(int length)
        {
            string message = "next";
            try
            {
                byte[] data = new byte[length]; // буфер для получаемых данных
                StringBuilder builder = new StringBuilder();
                int bytes = 0;
                do
                {
                    bytes = stream.Read(data, 0, data.Length);
                    builder.Append(Encoding.Unicode.GetString(data, 0, bytes));
                }
                while (stream.DataAvailable);

                message = builder.ToString();
                Console.WriteLine(message);//вывод сообщения
            }
            catch
            {
                Console.WriteLine("Подключение прервано!"); //соединение было прервано
                Disconnect();
            }
            return message;
        }
     
     
     */
