﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nextCheck.Classes
{
    public class User
    {
        public string name { get; }
        public string mail { get; }
        public string token { get; }

        public User(string name, string mail, string token)
        {
            this.name = name;
            this.mail = mail;
            this.token = token;
        }
    }
}
