﻿using Microsoft.Office.Interop.Excel;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Excel = Microsoft.Office.Interop.Excel;

namespace nextCheck.Classes
{
    class ExcelBook : IDisposable
    {
        public Excel.Application excelApp;
        public Excel.Workbook workBook;
        public Excel.Worksheet worksheet;
        public Excel.Range lastCell;

        public ExcelBook()
        {
            excelApp = new Excel.Application();
        }
        public void createFile()
        {
            workBook = excelApp.Workbooks.Add(Type.Missing);
        }

        public void openFile(string name, bool readOnly)
        {
            try
            {
                workBook = excelApp.Workbooks.Open(name,
                              Type.Missing, readOnly, Type.Missing, Type.Missing,
                              Type.Missing, true, Type.Missing, Type.Missing,
                              Type.Missing, Type.Missing, Type.Missing, Type.Missing,
                              Type.Missing, Type.Missing);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            worksheet = (Excel.Worksheet)workBook.Worksheets.get_Item(1);
        }

         public List<string> getHeaders()
        {
            List<string> headers = new List<string>();
            try
            {
                int clm = lastCell.Column;
                int row = lastCell.Row;
                object[,] arr = getValues("A1", worksheet.Cells[1,clm]);
                if (arr == null) throw new Exception("Empty Sheet");
                for (int i = 1; i <= clm; i++)
                {
                    if (arr[1, i] != null && !arr[1, i].ToString().Equals(""))
                    {                      
                        headers.Add(arr[1, i].ToString().ToLower());
                    }
                    else
                    {
                        string value = "";
                        object[,] arr2 = getValues(worksheet.Cells[1, i], worksheet.Cells[row, i]);
                        for (int j = 2; j <=row; j++)
                        {
                            if(arr2[j, 1] != null)
                            {
                                value = arr2[j, 1].ToString();
                                break;
                            }
                        }                        
                        headers.Add(value);
                    }
                }

            }
            catch(Exception ex)
            {
                throw ex;
            }

            return headers;
        }

        public List<string> getHeadersWithoutNull()
        {
            List<string> headers = new List<string>();
            try
            {
                int clm = lastCell.Column;

                object[,] arr = getValues("A1", worksheet.Cells[1, clm]);

                for (int i = 1; i <= clm; i++)
                {
                    if (arr[1, i] != null)
                    {
                        headers.Add(arr[1, i].ToString().ToLower());
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return headers;
        }

        public void saveFile(string path)
        {
            try
            {
                workBook.SaveAs(path, Excel.XlFileFormat.xlWorkbookDefault, Type.Missing, Type.Missing, false, Type.Missing, Excel.XlSaveAsAccessMode.xlNoChange, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void saveFile()
        {
            workBook.Save();
        }


        public object[,] getAllValues()
        {
            try
            {
                lastCell = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing);
                Excel.Range range = worksheet.get_Range("A1", lastCell);
                return range.Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't get values from sheet:" + worksheet.Name);
                Console.WriteLine(ex.ToString());
            }
            return null;
        }

        public object[,] getAllValues(string sheet)
        {
            try
            {
                setSheet(sheet);
                lastCell = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing);
                Excel.Range range = worksheet.get_Range("A1", lastCell);
                return range.Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Not found such sheet: " + sheet);
                Console.WriteLine(ex.ToString());
                return null;
            }

        }

        public void setValues(string[,] array,int row,int column)
        {
            if (row > 1000000)
            {
                int newRow = row - 1000000 + 1;
                string[,] array2 = new string[1000000, column];
                string[,] arrayNew = new string[newRow, column];
                for (int i = 0; i < row; i++)
                {
                    for (int j = 0; j < column; j++)
                    {
                        if (i == 0)
                        {
                            array2[i, j] = array[i, j];
                            arrayNew[i, j] = array[i, j];
                        }
                        else
                        if (i < 1000000)
                            array2[i, j] = array[i, j];
                        else
                            arrayNew[i - 999999, j] = array[i, j];
                    }
                }
                Excel.Range c1 = (Excel.Range)worksheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)worksheet.Cells[1000000, column];
                Excel.Range range = worksheet.get_Range(c1, c2);
                range.Value = array2;
                string name = worksheet.Name;
                addSheet(name + "-2");
                setSheet(name + "-2");
                c1 = (Excel.Range)worksheet.Cells[1, 1];
                c2 = (Excel.Range)worksheet.Cells[newRow, column];
                range = worksheet.get_Range(c1, c2);
                range.Value = arrayNew;
                setSheet(name);
            }
            else
            {
                Excel.Range c1 = (Excel.Range)worksheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)worksheet.Cells[row, column];
                Excel.Range range = worksheet.get_Range(c1, c2);
                range.Value = array;
            }
        }

        public void setValues(object[,] array, int row, int column)
        {
            if (row > 1000000)
            {
                int newRow = row - 1000000 + 1;
                object[,] array2 = new object[1000000, array.GetUpperBound(1)];
                object[,] arrayNew = new object[newRow, array.GetUpperBound(1)];
                for (int i = 0; i < array.GetUpperBound(0); i++)
                {
                    for (int j = 0; j < array.GetUpperBound(1); j++)
                    {
                        if (i == 0)
                        {
                            array2[i, j] = array[i, j];
                            arrayNew[i, j] = array[i, j];
                        }
                        else
                        if (i < 1000000)
                            array2[i, j] = array[i, j];
                        else
                            arrayNew[i - 999999, j] = array[i, j];
                    }
                }
                Excel.Range c1 = (Excel.Range)worksheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)worksheet.Cells[1000000, column];
                Excel.Range range = worksheet.get_Range(c1, c2);
                range.Value = array2;
                string name = worksheet.Name;
                addSheet(worksheet.Name + "-2");
                setSheet(worksheet.Name + "-2");
                c1 = (Excel.Range)worksheet.Cells[1, 1];
                c2 = (Excel.Range)worksheet.Cells[newRow, column];
                range = worksheet.get_Range(c1, c2);
                range.Value = arrayNew;
                range.NumberFormat = "@";
                range.Columns.AutoFit();
                setSheet(name);

            }
            else
            {
                Excel.Range c1 = (Excel.Range)worksheet.Cells[1, 1];
                Excel.Range c2 = (Excel.Range)worksheet.Cells[row, column];
                Excel.Range range = worksheet.get_Range(c1, c2);
                
                range.Value = array;
               
            }

        }

        public void setPartValues(object[,] array, int rowStart, int columnStart, int rowEnd, int columnEnd)
        {
          
            Excel.Range c1 = (Excel.Range)worksheet.Cells[rowStart, columnStart];
            Excel.Range c2 = (Excel.Range)worksheet.Cells[rowEnd, columnEnd];
            Excel.Range range = worksheet.get_Range(c1, c2);

            range.Value = array;
            c1 = (Excel.Range)worksheet.Cells[1, 1];
            c2 = (Excel.Range)worksheet.Cells[rowEnd, columnEnd];
            Excel.Range rangeall = worksheet.get_Range(c1, c2);
            rangeall.NumberFormat = "@";
            rangeall.ColumnWidth = 12;
        }

        public void setColor(int row, int column, int row2, int column2,Color color,Color fontColor)
        {
            Excel.Range c1 = (Excel.Range)worksheet.Cells[row, column];
            Excel.Range c2 = (Excel.Range)worksheet.Cells[row2, column2];
            Excel.Range range = worksheet.get_Range(c1, c2);
            range.Font.Color = System.Drawing.ColorTranslator.ToOle(fontColor);
            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(color);
        }


        public object[,] getValues(object c1, object c2)
        {
            try
            {
                Excel.Range range = worksheet.get_Range(c1, c2);
                return range.Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't take range");
                Console.WriteLine(ex.ToString());
                return null;
            }

        }


        public object[,] getValuesFromOneColumn(int column)
        {
            try
            {
                Excel.Range c1 = (Excel.Range)worksheet.Cells[1, column];
                Excel.Range c2 = (Excel.Range)worksheet.Cells[lastCell.Row, column];
                Excel.Range range = worksheet.get_Range(c1, c2);
                return range.Value;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Can't take range");
                Console.WriteLine(ex.ToString());
                return null;
            }

        }

        public void setSheet(string nameSheet)
        {
            bool found = false;
            foreach (Worksheet s in workBook.Sheets)
            {
                if (s.Name.Equals(nameSheet, StringComparison.OrdinalIgnoreCase))
                {
                    found = true;
                    worksheet = s;
                    lastCell = worksheet.Cells.SpecialCells(XlCellType.xlCellTypeLastCell, Type.Missing);
                }
            }
            if (!found) throw new Exception("Not found sheet:" + nameSheet);
        }

        public Sheet getSheet(string name)
        {
            try
            {
                Sheet sheet = new Sheet();
                sheet.name = name;
                Console.WriteLine("Set Sheet: " + name);
                setSheet(name);
                Console.WriteLine("Get Headers");
                sheet.headers = getHeaders();
                Console.WriteLine("Get All Value");
                sheet.values = getAllValues();    
                return sheet;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Prolbems with sheet:" + name, ex);
                return null;
            }
        }

        public Sheet getSheetOnlyHeader(string name)
        {
            try
            {
                Sheet sheet = new Sheet();
                sheet.name = name;
                Console.WriteLine("Set Sheet: " + name);
                setSheet(name);
                Console.WriteLine("Get Headers");
                sheet.headers = getHeaders();
                return sheet;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Prolbems with sheet:" + name, ex);
                return null;
            }
        }

        //public Sheet getSheet(string name)
        //{
        //    try
        //    {
        //        Sheet sheet = new Sheet();
        //        sheet.name = name;
        //        Console.WriteLine("Set Sheet: " + name);
        //        setSheet(name);
        //        Console.WriteLine("Get Headers");
        //        sheet.headers = getHeadersWithoutNull();
        //        Console.WriteLine("Get All Value");
        //        object[,] values = getAllValues();


        //        Row row;
        //        Cell cell;
        //        sheet.rows = new List<Row>();
        //        for (int i = 2; i <= lastCell.Row; i++)
        //        {
        //            row = new Row();
        //            row.cells = new List<Cell>();
        //            for (int j = 2; j <= lastCell.Column; j++)
        //            {
        //                if (values[1, j] != null && !values[1, j].ToString().Equals(""))
        //                {
        //                    cell = new Cell();
        //                    cell.header = values[1, j].ToString();
        //                    if(values[i, j]!=null)
        //                        cell.value = values[i, j].ToString();
        //                    else
        //                        cell.value = "";
        //                    if (cell.header.Equals("SKU", StringComparison.OrdinalIgnoreCase))
        //                    {
        //                        row.sku = cell.value;
        //                    }
        //                    row.cells.Add(cell);
        //                }
        //            }
        //            sheet.rows.Add(row);
        //        }
        //        return sheet;
        //    }
        //    catch(Exception ex)
        //    {
        //        throw new Exception("Prolbems with sheet:" + name,ex);
        //    }
        //}

        public List<string> getValuesFromColumn(Sheet sheet, string column, bool isDuplicate)
        {
            List<string> list = new List<string>();
            int brandIndex = sheet.headers.IndexOf(column) + 1;
            Console.WriteLine("Index: " + brandIndex);
            string value;
            int row = sheet.values.GetUpperBound(0);
            for (int i = 2; i <= row; i++)
            {
                if (sheet.values[i, brandIndex] != null)
                    value = sheet.values[i, brandIndex].ToString();
                else
                {
                    value = "EMPTY";
                    //MessageBox.Show("В чеке "+ sheet.values[i-1, brandIndex] + " - в колонке Бренд на ShouldBe пустое значение");
                }
                if (isDuplicate) list.Add(value);
                else
                if (!list.Contains(value)) list.Add(value);
            }
            Console.WriteLine("Count of values in Column: " + list.Count);
            return list;
        }

       

        public Excel.Worksheet addSheet(string name)
        {
            Excel.Worksheet sheet = (Excel.Worksheet)workBook.Worksheets.Add();
            sheet.Name = name;
            return sheet;
        }

        public void renameSheet(int index, string name)
        {
            workBook.Sheets[index].Name = name;
        }

        public void Dispose()
        {
            try
            {
                if(workBook!=null)
                    workBook.Close(false, "", true);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}





/*
 private List<Brand> getBrandsFromExcel()
        {
            try
            {
                if (System.IO.File.Exists(pathFiles + "brandList_old.xlsx"))
                {
                    System.IO.File.Delete(pathFiles + "brandList_old.xlsx");
                }
                if (System.IO.File.Exists(pathFiles + "brandList.xlsx"))
                {
                    System.IO.File.Copy(pathFiles + "brandList.xlsx", pathFiles + "brandList_old.xlsx", true);
                    //System.IO.File.Delete(pathFiles + "brandList.xlsx");
                }
                GoogleDrive gDrive = new GoogleDrive(this);
                gDrive.AuthenticateOauth();
                gDrive.DownloadFile("1CymVfZIci8-LSUCv4o-6isQl9kJwHv91", pathFiles + "brandList.xlsx");
                gDrive.dispose();
                addLogo("Successful: download Brand List");
            }
            catch(Exception ex)
            {
                addLogo("Failed: download Brand List");
                addLogo(ex.ToString());
            }
            List<Brand> listOfBrands = new List<Brand>();
            List<string> listOfNames = new List<string>();
            try
            {
                using (ExcelBook excelBook = new ExcelBook())
                {
                    addLogo("Loading: " + pathFiles + "brandList.xlsx");
                    excelBook.openFile(pathFiles + "brandList.xlsx", true);


                    object[,] allValueFromBook = excelBook.getAllValues();
                    int sizeCol = excelBook.lastCell.Column;
                    int sizeRow = excelBook.lastCell.Row;
                    List<string> clmnNames = new List<string>();
                    for (int i = 1; i <= sizeCol; i++)
                    {
                        if (allValueFromBook[1, i] != null)
                        {
                            clmnNames.Add(allValueFromBook[1, i].ToString());
                        }
                    }

                    Brand brand = null;
                    int errors = 0;
                    for (int i = 2; i <= sizeRow; i++)
                    {
                        if (allValueFromBook[i, clmnNames.IndexOf("Brand") + 1] != null && !listOfNames.Contains(allValueFromBook[i, clmnNames.IndexOf("Brand") + 1].ToString()))
                        {
                            brand = new Brand();
                            brand.name = allValueFromBook[i, clmnNames.IndexOf("Brand") + 1].ToString();
                            listOfNames.Add(brand.name);
                            brand.isChecking = false;
                            if (allValueFromBook[i, clmnNames.IndexOf("BrandID") + 1] != null)
                                brand.brandid = allValueFromBook[i, clmnNames.IndexOf("BrandID") + 1].ToString();
                            if (allValueFromBook[i, clmnNames.IndexOf("BrandFolderID") + 1] != null)
                                brand.folderID = allValueFromBook[i, clmnNames.IndexOf("BrandFolderID") + 1].ToString();
                            listOfBrands.Add(brand);
                        }
                        else
                        {
                            errors++;
                        }
                    }
                    addLogo("Successful", Colors.DarkGreen);
                    addLogo(@"Brands w/o errors: " + listOfBrands.Count, Colors.DarkGreen);
                    addLogo(@"Brands with errors: " + errors, Colors.DarkRed);

                }
            }
            catch (Exception ex)
            {
                addLogo("Can't open!");
                addLogo(ex.ToString(), Colors.DarkRed);
            }

            return listOfBrands;
        }
     
     */
