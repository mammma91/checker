﻿using LiteDB;
using Microsoft.Office.Interop.Excel;
using nextCheck.DataBase;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace nextCheck.Classes
{
    public class Check
    {
        private Brand brand { get; set; }
        private List<Brand> allBrands;
        public List<string> brandsInCheck;
        private string brandNameFromCheck = "";
        private string jobber = "";
        private string application = "";
        private bool foundBrandName;
        public List<Sheet> sheets;
        public Check(Brand brand)
        {
            this.brand = brand;
            brandsInCheck = new List<string>();
            sheets = new List<Sheet>();
            foundBrandName = false; 
        }

        public bool compareName()
        {
            try
            {
                foreach (string tempName in brandsInCheck)
                {
                    if (brand.name.IndexOf(tempName, StringComparison.OrdinalIgnoreCase) >= 0 || brand.nameInCheck.IndexOf(tempName, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        Console.WriteLine("Чек содержит " + tempName);
                        foundBrandName = true;
                        brand.nameInCheck = tempName;
                    }
                    else
                    {
                        try
                        {
                            DateTime t = DateTime.Now;
                            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                            {
                                var col = db.GetCollection<DataBase.Brand>("brands");
                                col.EnsureIndex(x => x.name);
                                Brand tempBrand = col.FindOne(x => x.name.Equals(tempName));
                                DateTime t2 = DateTime.Now;
                                TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
                                Console.WriteLine("Find Brand: " + (elapsedSpan.TotalSeconds));
                                if (tempBrand != null)
                                {
                                    if (tempBrand.time.CompareTo(brand.time) < 0 && tempBrand.folderID != null && !tempBrand.folderID.Equals(""))
                                    {
                                        Console.WriteLine("Update: " + tempName);
                                        tempBrand.nameInCheck = tempName;
                                        tempBrand.jobber = jobber;
                                        tempBrand.application = application;
                                        tempBrand.time = brand.time;
                                        tempBrand.timeCreate = brand.timeCreate;
                                        tempBrand.dateTime = brand.dateTime;
                                        tempBrand.checkID = brand.checkID;
                                        tempBrand.checkName = brand.checkName;
                                        tempBrand.link = brand.link;
                                        tempBrand.owner = brand.owner;
                                        col.Update(tempBrand);
                                        foreach (Sheet sheet in sheets)
                                        {
                                            using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB+ sheet.name.Replace(" ", "")+".db")) { 
                                                var colSheet = dbsheet.GetCollection<Row>("sheet");
                                                Console.WriteLine(sheet.name);
                                                colSheet.EnsureIndex(x => x.brand);
                                                colSheet.DeleteMany(x => x.brand.Equals(tempBrand.nameInCheck, StringComparison.OrdinalIgnoreCase));
                                                colSheet.Insert(new Row() { sku = "HEADER", brand = tempBrand.nameInCheck, brandNameFromFile = tempBrand.name, cells = sheet.headers });
                                            }
                                            if(sheet.name.Equals("SKUs missing on CARiD", StringComparison.OrdinalIgnoreCase))
                                            {
                                                using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "allMissing.db"))
                                                {
                                                    var colSheet = dbsheet.GetCollection<Row>("Missing");
                                                    colSheet.EnsureIndex(x => x.brand);
                                                    colSheet.DeleteMany(x => x.brand.Equals(tempBrand.nameInCheck, StringComparison.OrdinalIgnoreCase));
                                                    colSheet.InsertBulk(sheet.getRows(tempBrand.nameInCheck));
                                                   
                                                }
                                            }
                                        }                                     
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                        }
                    }
                }
            }
            catch
            {
                return false;
            }

            if (!foundBrandName)
                brand.error = "В чеке отсутствует данный бренд, проверте правильность folder ID";
            else
                brand.error = "";
            return foundBrandName;
        }


        public void getValuesFromCheck()
        {         
            using (ExcelBook check = new ExcelBook())
            {
                try
                {
                    Console.WriteLine("Open check: " + MainWindow.pathChecks + brand.checkName);
                    check.openFile(MainWindow.pathChecks + brand.checkName, true);
                    DateTime t = DateTime.Now;
                    check.setSheet("General");
                    int c = 1;
                    if ("brand".Equals(check.worksheet.Cells[4, 1].Value, StringComparison.OrdinalIgnoreCase) || "brands".Equals(check.worksheet.Cells[4, 1].Value, StringComparison.OrdinalIgnoreCase)) c = 2;
                    else
                        if ("brand".Equals(check.worksheet.Cells[4, 2].Value, StringComparison.OrdinalIgnoreCase) || "brands".Equals(check.worksheet.Cells[4, 2].Value, StringComparison.OrdinalIgnoreCase)) c = 3;
                    brandNameFromCheck = check.worksheet.Cells[4, c].Value;
                    jobber = check.worksheet.Cells[7, c].Value;
                    application = check.worksheet.Cells[8, c].Value;
                    brand.jobber = jobber;
                    brand.application = application;
                  
                    Sheet sheet = check.getSheet("SKUs should be on the website");
                    if (!sheet.headers.Contains("brand"))
                    {
                        Console.WriteLine("No column 'Brand'");
                        brandsInCheck.Add(brandNameFromCheck);
                        sheet.brands = brandsInCheck;
                    }
                    else
                    {
                        sheet.brands = check.getValuesFromColumn(sheet, "brand", false);
                        brandsInCheck = sheet.brands;
                    }
                    
                    Console.WriteLine("Brands in check: " + brandsInCheck.Count);
                    sheets.Add(sheet);

                    try
                    {
                        sheet = check.getSheet("SKUs missing on CARiD");
                        if (sheet==null || sheet.headers.Count==0) throw new Exception("Empty");
                        if (!sheet.headers.Contains("brand"))
                        {
                            sheet.brands = brandsInCheck;
                        }
                        else
                            sheet.brands = check.getValuesFromColumn(sheet, "brand", false);
                        if (sheet.headers.Contains("importername"))
                        {
                            List<string> nameList = check.getValuesFromColumn(sheet, "importername", false);
                            using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "importers.db"))
                            {
                                var colSheet = dbsheet.GetCollection<Importer>("Importer");
                                colSheet.EnsureIndex(x => x.name);
                                foreach(String name in nameList)
                                {
                                    List<string> list;
                                    if (name.Contains(";")) {
                                        list = name.Split(';').ToList();
                                    }
                                    else
                                    {
                                        list = new List<string>();
                                        list.Add(name);
                                    }
                                    foreach (string str in list)
                                    {
                                        if (str.Contains("."))
                                        {
                                            string n = str.Replace("\"", "");
                                            n = n.Substring(0, n.IndexOf("."));
                                            if (!colSheet.Exists(x => x.name.Equals(n, StringComparison.OrdinalIgnoreCase))) colSheet.Insert(new Importer() { name = n, select = false });
                                        }
                                    }
                                }
                            }
                        }
                        Console.WriteLine("Add sheet: Missing");
                        sheets.Add(sheet);
                    }catch(Exception ex)
                    {
                        Console.WriteLine("Missing problems: " + ex);
                    }


                    foreach (Worksheet s in check.workBook.Sheets)
                    {
                        if (!s.Name.Equals("General", StringComparison.OrdinalIgnoreCase) && !s.Name.Equals("SKUs should be on the website", StringComparison.OrdinalIgnoreCase) && !s.Name.Equals("SKUs missing on CARiD", StringComparison.OrdinalIgnoreCase) && !s.Name.Equals(""))
                        {
                            sheet = check.getSheetOnlyHeader(s.Name);
                            if (sheet != null) sheets.Add(sheet);
                            Console.WriteLine("Add sheet: " + s.Name);
                        }
                    }
                    //sheet = check.getSheetOnlyHeader("SKUs missing on CARiD");
                    //if (sheet != null) sheets.Add(sheet);
                    //sheet = check.getSheet("Application");
                    //if (sheet != null) sheets.Add(sheet);
                    //sheet = check.getSheetOnlyHeader("NEED RENAME");
                    //if (sheet != null) sheets.Add(sheet);

                    DateTime t2 = DateTime.Now;
                    TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
                    Console.WriteLine("Get sheet Time: " + (elapsedSpan.TotalSeconds));
                    //Sheet sheet = check.getSheet("SKUs should be on the website");
                    //sheets.Add(sheet);
                    //if (!sheet.headers.Contains("Brand")) throw new Exception("No column 'Brand'");
                    //brandsInCheck = check.getValuesFromColumn(sheet, "Brand", false);
                    //Console.WriteLine("brandsInCheck - " + brandsInCheck.Count);
                    //try
                    //{
                    //    sheet = check.getSheet("SKUs missing on CARiD");
                    //    sheets.Add(sheet); 
                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine(ex.ToString());
                    //    MessageBox.Show("Ошибка в чтении страницы чека"+ex.ToString());
                    //}
                    //try
                    //{
                    //    sheet = check.getSheet("Application");
                    //    sheets.Add(sheet);
                    //}
                    //catch (Exception ex)
                    //{
                    //    Console.WriteLine(ex.ToString());
                    //    MessageBox.Show("Ошибка в чтении страницы чека" + ex.ToString());
                    //}

                    //Console.WriteLine("Open - SKUs should be on the website");
                    //check.setSheet("SKUs should be on the website");
                    //Console.WriteLine("Get Headers");
                    //List<string> headers = check.getHeaders();
                    //if (!headers.Contains("Brand")) throw new Exception("No column 'Brand'");
                    //int brandIndex = headers.IndexOf("Brand")+1;
                    //Console.WriteLine("Index: "+brandIndex);
                    //object[,] brands = check.getValuesFromOneColumn(brandIndex);
                    //string value;
                    //int k = 0;
                    //Console.WriteLine("Brands length: " + brands.Length);
                    //for (int i = 2; i <= brands.Length; i++)
                    //{
                    //    value = brands[i, 1].ToString();
                    //    if (!brandsInCheck.Contains(value))
                    //    {
                    //        brandsInCheck.Add(value);
                    //    }
                    //    k++;
                    //}


                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed: from check to BD");
                    Console.WriteLine(ex.ToString());
                    brand.error = "Failed: get info from check";
                }
            }
        }

        //public Sheet getValuesFromSheet(string name)
        //{
        //    try
        //    {
        //        Sheet sheet = new Sheet();
        //        sheet.name = name;
        //        Console.WriteLine("Set Sheet: " + name);
        //        setSheet(name);
        //        Console.WriteLine("Get Headers");
        //        sheet.headers = getHeaders();
        //        Console.WriteLine("Get All Value");
        //        sheet.values = getAllValues();
        //        return sheet;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Prolbems with sheet:" + name, ex);
        //    }
        //}

    }
}



/*
 * 
 * 
 * 
    public bool compareName()
        {
            using (ExcelBook check = new ExcelBook())
            {
                try
                {
                    check.openFile(MainWindow.pathChecks + brand.checkName, true);
                    check.setSheet("General");
                    brandNameFromCheck = check.worksheet.Cells[4, 3].Value;
                    jobber = check.worksheet.Cells[7, 3].Value;
                    application = check.worksheet.Cells[8, 3].Value;

                    if (brandNameFromCheck.Contains(", ") || brandNameFromCheck.Contains(@" / ") || brandNameFromCheck.Contains(@" \ "))
                    {
                        string[] brandsNames = null;
                        if (brandNameFromCheck.Contains(", ")) brandsNames = brandNameFromCheck.Split(',');
                        else
                        if (brandNameFromCheck.Contains(" / ")) brandsNames = brandNameFromCheck.Split('/');
                        else
                        if (brandNameFromCheck.Contains(@" \ ")) brandsNames = brandNameFromCheck.Split('\\');

                        foreach (string n in brandsNames)
                        {
                            string tempName = n;
                            if (tempName.LastIndexOf(" ") == tempName.Length - 1)
                                tempName = tempName.Remove(tempName.LastIndexOf(" "), 1);
                            if (tempName.IndexOf(" ") == 0)
                                tempName = tempName.Remove(0, 1);
                            brandsInCheck.Add(tempName);

                            if (brand.name.IndexOf(tempName, StringComparison.OrdinalIgnoreCase) >= 0 || brand.nameInCheck.IndexOf(tempName, StringComparison.OrdinalIgnoreCase) >= 0)
                            {
                                //addLogo("Чек содержит " + tempName);
                                foundBrandName = true;
                                brand.nameInCheck = tempName;
                            }                           
                        }
                    }
                    else
                    {
                        brandsInCheck.Add(brandNameFromCheck);
                        if (brand.name.IndexOf(brandNameFromCheck, StringComparison.OrdinalIgnoreCase) >= 0 || brand.nameInCheck.IndexOf(brandNameFromCheck, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            foundBrandName = true;
                            brand.nameInCheck = brandNameFromCheck;
                        }
                    }
                }
                catch(Exception ex)
                {
                    //addLogo(ex.ToString());
                }
            }

            if (!foundBrandName)
            {
               
            }

            return foundBrandName;
        }
     





        public bool compareName()
        {
            //using (ApplicationContext db = new ApplicationContext())
            //{
            //    allBrands = db.Brands.ToList();
            //}
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
            {
                allBrands = db.GetCollection<DataBase.Brand>("brands").FindAll().ToList();
            }

            try
            {
                if (brandNameFromCheck.Contains(", ") || brandNameFromCheck.Contains(@" / ") || brandNameFromCheck.Contains(@" \ "))
                {
                    string[] brandsNames = null;
                    if (brandNameFromCheck.Contains(", ")) brandsNames = brandNameFromCheck.Split(',');
                    else
                    if (brandNameFromCheck.Contains(" / ")) brandsNames = brandNameFromCheck.Split('/');
                    else
                    if (brandNameFromCheck.Contains(@" \ ")) brandsNames = brandNameFromCheck.Split('\\');
                    foreach (string n in brandsNames)
                    {
                        string tempName = n;

                        if (tempName.LastIndexOf(" ") == tempName.Length - 1)
                            tempName = tempName.Remove(tempName.LastIndexOf(" "), 1);
                        if (tempName.IndexOf(" ") == 0)
                            tempName = tempName.Remove(0, 1);
                        brandsInCheck.Add(tempName);

                        if (brand.name.IndexOf(tempName, StringComparison.OrdinalIgnoreCase) >= 0 || brand.nameInCheck.IndexOf(tempName, StringComparison.OrdinalIgnoreCase) >= 0)
                        {
                            Console.WriteLine("Чек содержит " + tempName);
                            foundBrandName = true;
                            brand.nameInCheck = tempName;
                        }
                        else
                        {
                            try
                            {
                                using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                                {
                                    var col = db.GetCollection<DataBase.Brand>("brands");
                                    Brand tempBrand = col.FindOne(x => x.name.Equals(tempName));
                                    if (tempBrand!=null)//(db.Brands.Contains(tempBrand))
                                    {
                                        //tempBrand = db.Brands.Find(tempBrand.name);

                                        if (tempBrand.time.CompareTo(brand.time) <0 && tempBrand.folderID!=null && !tempBrand.folderID.Equals(""))
                                        {
                                            Console.WriteLine("Update: "+ tempName);
                                            tempBrand.nameInCheck = tempName;
                                            tempBrand.jobber = jobber;
                                            tempBrand.application = application;
                                            tempBrand.time = brand.time;
                                            tempBrand.timeCreate = brand.timeCreate;
                                            tempBrand.dateTime = brand.dateTime;
                                            tempBrand.checkID = brand.checkID;
                                            tempBrand.checkName = brand.checkName;
                                            tempBrand.link = brand.link;
                                            tempBrand.owner = brand.owner;
                                            col.Update(tempBrand);
                                            //db.Brands.Update(tempBrand);
                                            //db.SaveChanges();
                                        }
                                    }
                                    //else
                                    //{
                                    //    foreach(Brand br in col.FindAll().ToList()) //(Brand br in db.Brands.ToList())
                                    //    {
                                    //        if (br.nameInCheck != null && br.nameInCheck.Equals(tempName, StringComparison.OrdinalIgnoreCase))
                                    //        {
                                    //            if (br.timeChange.CompareTo(brand.timeChange) <0)
                                    //            {
                                    //                tempBrand.name = br.name;
                                    //                tempBrand.brandid = br.brandid;
                                    //                tempBrand.isChecking = br.isChecking;
                                    //                tempBrand.nameInCheck = tempName;
                                    //                tempBrand.jobber = jobber;
                                    //                tempBrand.application = application;
                                    //                tempBrand.timeChange = brand.timeChange;
                                    //                tempBrand.dateTime = brand.dateTime;
                                    //                tempBrand.checkName = brand.checkName;
                                    //                tempBrand.link = brand.link;
                                    //                tempBrand.owner = brand.owner;
                                    //                col.Update(tempBrand);
                                    //                //db.Brands.Update(tempBrand);
                                    //                //db.SaveChanges();
                                    //                if (tempBrand.name.Equals(brand.name, StringComparison.OrdinalIgnoreCase))
                                    //                {
                                    //                    foundBrandName = true;
                                    //                    brand.set(tempBrand);
                                    //                    Console.WriteLine("Contains" + tempBrand.name);
                                    //                }
                                    //            }
                                    //        }
                                    //    }

                                    //}


                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.ToString());
                            }
                        }

                    }

                }
                else
                {
                    brandsInCheck.Add(brandNameFromCheck);
                    if (brand.name.IndexOf(brandNameFromCheck, StringComparison.OrdinalIgnoreCase) >= 0 || brand.nameInCheck.IndexOf(brandNameFromCheck, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        foundBrandName = true;
                        brand.nameInCheck = brandNameFromCheck;
                    }
                }
            }
            catch
            {
                return false;
            }

            if (!foundBrandName)
                brand.error = "В чеке отсутствует данный бренд, проверте правильность folder ID";
            else
                brand.error = "";

            return foundBrandName;
        }

     */
