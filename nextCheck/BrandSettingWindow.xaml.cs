﻿using LiteDB;
using nextCheck.Classes;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace nextCheck
{
    /// <summary>
    /// Interaction logic for BrandSettingWindow.xaml
    /// </summary>
    public partial class BrandSettingWindow : Window
    {

        private Brand brand;
        public BrandSettingWindow(Brand brand)
        {
            InitializeComponent();
            this.brand = brand;
            brandNamebox.Text = brand.name;
            //brandNamebox.IsEnabled = false;
            brandNameInCheckBox.Text = brand.nameInCheck;
            brandIdBox.Text = brand.brandid;
            folderIdBox.Text = brand.folderID;
            if (brand.folderID != null && !brand.folderID.Equals(""))
                folderLinkBox.Text = "https://drive.google.com/open?id=" + brand.folderID;
            if(brand.link!=null)
                checkLinkBox.Text = brand.link;
            if (brand.checkName != null && !brand.checkName.Equals(""))
                checkNameBox.Text = brand.checkName;
            if (brand.owner != null && !brand.owner.Equals(""))
                ownBox.Text = brand.owner;
            if (brand.dateTime != null && !brand.dateTime.Equals(""))
                dateBox.Text = brand.dateTime;
            if (brand.jobber != null && !brand.jobber.Equals(""))
                jobberBox.Text = brand.jobber;
            if (brand.application != null && !brand.application.Equals(""))
                appBox.Text = brand.application;
            if (brand.error != null && !brand.error.Equals(""))
                errorBox.Text = brand.error;
        }

        private void doubleClick_HyperLink(object sender, RoutedEventArgs e)
        {
            try
            {
                var url = ((TextBox)sender).Text;

                Process.Start(url);
            }
            catch (Exception ex)
            {

            }
        }

        private void yes_click(object sender, RoutedEventArgs e)
        {
            if (brandNamebox.Text != "")
            {
                if (!brandNamebox.Text.Equals(brand.name))
                {
                    using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                    {
                        var col = db.GetCollection<DataBase.Brand>("brands");
                        Brand b = col.FindOne(x => x.name.Equals(brandNamebox.Text));
                        if (b!=null) // (db.Brands.Find(brandNamebox.Text) != null)
                        {
                            MessageBox.Show("Бренд не может быть переименован, т.к. бренд с таким именем уже существует в БД.");
                            return;
                        }
                        else
                        {
                            // brand.isChecking = false;
                            b.isChecking = false;
                            col.Update(b);
                            //db.Brands.Update(brand);
                            //db.SaveChanges();
                            brand.name = brandNamebox.Text;
                            brand.isChecking = true;
                            col.Insert(brand);
                            //db.Brands.Add(brand);
                            //db.SaveChanges();
                        }

                    }
                }
                brand.nameInCheck = brandNameInCheckBox.Text;
                brand.brandid = brandIdBox.Text;
                brand.error = errorBox.Text;
                if (jobberBox.Text.Equals("") || appBox.Text.Equals(""))
                {
                    brand.jobber = "";
                    brand.application = "";
                }
                if (dateBox.Text.Equals("") || checkNameBox.Text.Equals("") || folderIdBox.Text.Equals("") || !folderIdBox.Text.Equals(brand.folderID))
                {
                    brand.folderID = folderIdBox.Text;
                    brand.dateTime = "";
                    brand.time = 0;
                    brand.timeCreate = 0;
                    brand.checkName = "";
                    brand.link = "";
                    brand.owner = "";
                    brand.jobber = "";
                    brand.application = "";
                    brand.checkID = "";
                    brand.error = "";
                }


                using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                {
                    var col = db.GetCollection<DataBase.Brand>("brands");
                    col.Update(brand);
                    //db.Brands.Update(brand);
                    //db.SaveChanges();
                }


                this.DialogResult = true;
            }
            else
            {
                popup1.IsOpen = true;
            }
        }
        private void no_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
