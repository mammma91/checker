﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace nextCheck
{
    /// <summary>
    /// Interaction logic for WaitCloseExcel.xaml
    /// </summary>
    public partial class WaitCloseExcel : Window
    {
        public WaitCloseExcel()
        {
            InitializeComponent();

            warningWindow.Icon = new BitmapImage(new Uri(MainWindow.pathImg + @"checker_32px.ico"));

        }

        private void yes_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }
        private void no_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
