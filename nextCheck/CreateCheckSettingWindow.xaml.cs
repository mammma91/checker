﻿using LiteDB;
using nextCheck.Classes;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace nextCheck
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class CreateCheckWindow : Window
    {
        ObservableCollection<string> newHeaders;
        Dictionary<string, ComboBox> columns;
        Dictionary<Header, CheckBox> checkBoxes;
        List<BrandView> brandViewList;
        List<Header> listHeaders;
        MainWindow main;
        
        List<Sheet> sheets;
        public CreateCheckWindow(MainWindow main)
        {
            InitializeComponent();
            this.main = main;
            this.brandViewList = MainWindow.brandViewList.brandList;
            columns = new Dictionary<string, ComboBox>();
            checkBoxes = new Dictionary<Header, CheckBox>();
            newHeaders = new ObservableCollection<string>();
            listHeaders = new List<Header>();
            sheets = new List<Sheet>();

        }

        public void addColumn(Header h)
        {
            TextBox textBox = new TextBox();
            textBox.IsReadOnly = true;
            textBox.Text = h.OldName;
            textBox.Foreground = new SolidColorBrush() { Color = Colors.White };
            ComboBox comboBox = new ComboBox();
            comboBox.ItemsSource = newHeaders;
            comboBox.IsEditable = true;
            comboBox.IsReadOnly = true;
            if (h.NewName != null && !h.NewName.Equals(""))
            {
                textBox.Background = new SolidColorBrush() { Color = Colors.Green, Opacity = 0.5 };
                comboBox.SelectedItem = h.NewName;
            }
            else
            {
                if(!newHeaders.Contains(h.OldName))
                    newHeaders.Add(h.OldName);
                textBox.Background = new SolidColorBrush() { Color = Colors.Red, Opacity = 0.8 };
                comboBox.SelectedItem = h.OldName;
            }
            Grid grid = new Grid();
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            Grid.SetColumn(textBox, 0);
            Grid.SetColumn(comboBox, 1);
            grid.Children.Add(textBox);
            grid.Children.Add(comboBox);
            mapList.Children.Add(grid);
            columns.Add(h.OldName,comboBox);
        }

        public void setHeaders(Sheet sheet)
        {
            try
            {
                sheet.headersDB = new List<Row>();
                using (LiteDatabase db = new LiteDatabase(MainWindow.pathFolderDB + sheet.name.Replace(" ", "") + ".db"))
                {
                    var colSheet = db.GetCollection<DataBase.Row>("sheet");
                    colSheet.EnsureIndex(x => x.sku);
                    List<Row> temp = colSheet.Find(x => x.sku.Equals("HEADER")).ToList();
                    foreach (BrandView brandView in brandViewList)
                    {
                        if (brandView.isChecked && temp.Exists(x=>x.brand.Equals(brandView.brand.nameInCheck,StringComparison.OrdinalIgnoreCase)))
                        {
                            sheet.headersDB.Add(temp.Find(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)));
                        }
                    }
                }
                List<string> allHeaders = new List<string>();
                foreach (Row r in sheet.headersDB)
                {
                    
                    foreach (string h in r.cells)
                    {
                        if (!allHeaders.Contains(h) && h!=null && !h.Equals(""))
                        {
                            Console.Write(h);
                            allHeaders.Add(h);
                        }
                    }                   
                }
                allHeaders.Sort();
                Console.WriteLine();
                
                using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                {
                    var colSheet = db.GetCollection<DataBase.Header>("Headers");
                    colSheet.EnsureIndex(x => x.OldName);
                    List<Header> headersDB = colSheet.FindAll().ToList();
                    foreach (string h in allHeaders)
                    {
                        Header header = colSheet.FindOne(x => x.OldName.Equals(h, StringComparison.OrdinalIgnoreCase)&&x.Sheet.Equals(sheet.name,StringComparison.OrdinalIgnoreCase));
                        if (header == null)
                        {
                            header = new Header();
                            header.Sheet = sheet.name;
                            header.OldName = h;
                            header.NewName = "";
                            header.ignore = false;
                            colSheet.Insert(header);
                        }
                        listHeaders.Add(header);
                    }
                }
                Console.WriteLine("Headers "+sheet.name+" count:"+listHeaders.Count);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
                this.DialogResult = false;
            }
        }

       

        private void CreateColumn_Click(object sender, RoutedEventArgs e)
        {
            string t = columnTextBox.Text;
            if(t!=null && !t.Equals("") && !newHeaders.Contains(t))
                newHeaders.Add(t);
            columnTextBox.Text = "";
        }

        public void createSheet(string name)
        {
            Sheet sheet = new Sheet() { name = name };
            setHeaders(sheet);
            sheets.Add(sheet);
        }

        public void getNewHeaders()
        {
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
            {
                var colSheet = db.GetCollection<DataBase.Header>("Headers");
                List<Header> headersDB = colSheet.FindAll().ToList();
                foreach (Header h in headersDB)
                {
                    if (h.NewName != null && !h.NewName.Equals("") && !newHeaders.Contains(h.NewName)) newHeaders.Add(h.NewName);
                }
                //colSheet.DeleteAll();
            }
        }

        public void clear()
        {
            newHeaders.Clear();
            columns.Clear();
            sheets.Clear();
            listHeaders.Clear();
            mapList.Children.Clear();
        }

        public void saveMapping()
        {
            foreach (Header h in listHeaders)
            {
               h.NewName = columns[h.OldName].Text;
            }
        }

        public void saveChecking()
        {
            foreach (Header h in listHeaders)
            {
                foreach (KeyValuePair<Header, CheckBox> d in checkBoxes)
                {
                    if (d.Key.NewName.Equals(h.NewName)&& d.Key.Sheet.Equals(h.Sheet))
                    {
                        h.ignore = !(bool)d.Value.IsChecked;
                        break;
                    }
                }
                //h.ignore = !(bool)checkBoxes[h].IsChecked;
                //Console.WriteLine(h.Sheet + " - " + h.OldName + " = " + h.NewName +" : IGNORE = "+h.ignore);
            }
        }

        public void saveColumnsToBD()
        {
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
            {
                var colSheet = db.GetCollection<DataBase.Header>("Headers");
                colSheet.EnsureIndex(x => x.OldName);
                foreach (Header h in listHeaders)
                {
                    List<Header> headers = colSheet.Find(x => x.OldName.Equals(h.OldName, StringComparison.OrdinalIgnoreCase)).ToList();
                    foreach(Header h2 in headers)
                    {
                        if (h2.Sheet.Equals(h.Sheet))
                        {
                            h2.NewName = h.NewName;
                            h2.ignore = h.ignore;
                            colSheet.Update(h2);
                        }
                    }
                }
            }
        }

        public void createCheckBoxes()
        {
            foreach(Sheet sheet in sheets)
            {
                GroupBox groupBox = new GroupBox() { Header = sheet.name, Padding=new Thickness(5), Foreground=new SolidColorBrush() {Color=Colors.White } };
                WrapPanel wrap = new WrapPanel() { ItemHeight = 30, ItemWidth = 150, Orientation = Orientation.Horizontal };
                foreach (Header h in listHeaders)
                {
                    if (h.Sheet.Equals(sheet.name,StringComparison.OrdinalIgnoreCase)) {
                        bool contains = false;
                        foreach (Header newH in checkBoxes.Keys)
                        {
                            if (newH.Sheet.Equals(h.Sheet) && newH.NewName.Equals(h.NewName)) {
                                contains = true;
                                break;
                            }
                        }
                        if (!contains)
                        {
                            CheckBox checkBox = new CheckBox() { IsChecked = !h.ignore, Content = h.NewName, Margin = new Thickness(3), Foreground = new SolidColorBrush() { Color = Colors.White } };
                            wrap.Children.Add(checkBox);
                            checkBoxes.Add(h, checkBox);
                        }
                    }
                }
                groupBox.Content = wrap;
                stackPanelCheck.Children.Add(groupBox);

            }
        }

        public List<Row> getRowsFromDB(string sheet)
        {
            List<Row> list = new List<Row>();
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathFolderDB + sheet.Replace(" ", "") + ".db"))
            {
                var colSheet = db.GetCollection<DataBase.Row>("sheet");
                colSheet.EnsureIndex(x => x.brand);
                foreach (BrandView brandView in brandViewList)
                {
                    if (brandView.isChecked && colSheet.Exists(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)))
                    {
                        List<Row> rows = colSheet.Find(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)).Where(x => !x.sku.Equals("HEADER")).ToList();
                        foreach (Row row in rows)
                        {
                            //row.brandNameFromFile = brandView.brand.name;

                            Row r = new Row() {brand=row.brand, brandNameFromFile= brandView.brand.name, sku=row.brand};
                            r.cells = row.cells.ToList();
                            list.Add(r);
                        }
                        
                    }
                }
            }
            return list;
        }

        public void createFile()
        {
            try
            {
                DateTime t = DateTime.Now;
                string date = "";
                Ookii.Dialogs.Wpf.VistaFolderBrowserDialog openDialog;
                using (ExcelBook book = new ExcelBook())
                {
                    book.createFile();
                    foreach (Sheet sheet in sheets)
                    {
                        book.addSheet(sheet.name);
                        book.setSheet(sheet.name);
                        sheet.headers = new List<string>();
                        List<Header> headersAll = listHeaders.FindAll(x => x.Sheet.Equals(sheet.name, StringComparison.OrdinalIgnoreCase) && !x.ignore);
                        foreach (Header h in headersAll)
                        {
                            if (!sheet.headers.Contains(h.NewName))
                                sheet.headers.Add(h.NewName);
                        }
                        sheet.headers.Sort();
                        int column = sheet.headers.Count();
                        Console.WriteLine("Columns in " + sheet.name + " : " + column);
                        if (column != 0)
                        {
                            sheet.rows = getRowsFromDB(sheet.name);
                            int row = sheet.rows.Count;
                            Console.WriteLine("Rows in " + sheet.name + " : " + row);
                            sheet.values = new string[row + 1, column+2];
                            sheet.values[0, 0] = "Key(Brand®SKU)";
                            sheet.values[0, 1] = "Brand(your_name)";
                            for (int i = 2; i < column+2; i++)
                            {
                                sheet.values[0, i] = sheet.headers[i-2];
                            }
                            int index = 1;
                            bool any;
                            foreach (Row brandHeader in sheet.headersDB)
                            {
                                List<Row> brandRows = sheet.rows.FindAll(x => x.brand.Equals(brandHeader.brand));
                                any = false;
                                for (int i = 0; i < brandHeader.cells.Count; i++)
                                {
                                    Header header = listHeaders.Find(x => x.OldName.Equals(brandHeader.cells[i], StringComparison.OrdinalIgnoreCase) && x.Sheet.Equals(sheet.name, StringComparison.OrdinalIgnoreCase));
                                    if (header != null && !header.ignore)
                                    {
                                        int c = sheet.headers.IndexOf(header.NewName)+2;
                                        if (!any)
                                        {
                                            for (int j = 0; j < brandRows.Count; j++)
                                            {
                                                sheet.values[index + j, 0] = brandRows[j].brandNameFromFile+ "®" + brandRows[j].sku;
                                                sheet.values[index + j, 1] = brandRows[j].brandNameFromFile;
                                            }
                                        }
                                        for (int j = 0; j < brandRows.Count; j++)
                                        {
                                            if (sheet.values[index + j, c] != null && !sheet.values[index + j, c].Equals(""))
                                            {
                                                if(brandRows[j].cells[i]!=null && !brandRows[j].cells[i].Equals(""))
                                                    sheet.values[index + j, c] += "|" + brandRows[j].cells[i];
                                            }
                                            else
                                                sheet.values[index + j, c] = brandRows[j].cells[i];
                                        }
                                        any = true;
                                    }
                                }
                                if (any)
                                    index += brandRows.Count;
                            }
                            book.setValues(sheet.values, row + 1, column+2);
                            book.setColor(1, 1, 1, 2, System.Drawing.Color.DarkOliveGreen, System.Drawing.Color.White);
                            book.setColor(1, 3, 1, column+2, System.Drawing.Color.FromArgb(169,208,142), System.Drawing.Color.Black);
                        }
                    }
                    DateTime t2 = DateTime.Now;
                    TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
                    Console.WriteLine("All Time: " + (elapsedSpan.TotalSeconds));
                    date = DateTime.Now.ToString().Replace(' ', '_').Replace(':', '-');
                    openDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                    if (openDialog.ShowDialog() != true)
                    {
                        Console.WriteLine("FolderError");
                        return;
                    }
                    book.saveFile(openDialog.SelectedPath + @"\One_Check_" + date + ".xlsx");
                }


                if (System.IO.File.Exists(openDialog.SelectedPath + @"\One_Check_" + date + ".xlsx"))
                {
                    Process myProcess = new Process();
                    myProcess.StartInfo.WorkingDirectory = MainWindow.pathChecks;
                    myProcess.StartInfo.FileName = openDialog.SelectedPath + @"\One_Check_" + date + ".xlsx";
                    myProcess.Start();
                }
            }catch(Exception ex)
            {
                Console.WriteLine("Create File Error: " + ex);
                MessageBox.Show(ex.ToString());
            }
        }

      

        public void createFileFromExcel()
        {
            try
            {
                string date = "";
               
                Ookii.Dialogs.Wpf.VistaFolderBrowserDialog openDialog;
                using (ExcelBook book = new ExcelBook())
                {
                    book.createFile();
                    book.addSheet("General");
                    book.setSheet("General");
                    book.worksheet.Cells[1, 1] = "Brand";
                    book.worksheet.Cells[1, 2] = "Check";
                    book.worksheet.Cells[1, 3] = "Jobber";
                    book.worksheet.Cells[1, 4] = "Application";
                    book.worksheet.Cells[1, 5] = "Time";
                    foreach (Sheet sheet in sheets)
                    {
                        book.addSheet(sheet.name);
                        book.setSheet(sheet.name);
                        sheet.headers = new List<string>();
                        List<Header> headersAll = listHeaders.FindAll(x => x.Sheet.Equals(sheet.name, StringComparison.OrdinalIgnoreCase) && !x.ignore);
                        foreach (Header h in headersAll)
                        {
                            if (!sheet.headers.Contains(h.NewName))
                            {
                                sheet.headers.Add(h.NewName);
                            }
                                
                        }
                        sheet.headers.Sort();
                        try
                        {
                            sheet.headers[sheet.headers.IndexOf("brand")] = sheet.headers[0];
                            sheet.headers[0] = "brand";
                            sheet.headers[sheet.headers.IndexOf("sku")] = sheet.headers[1];
                            sheet.headers[1] = "sku";
                            sheet.headers[sheet.headers.IndexOf("product description")] = sheet.headers[2];
                            sheet.headers[2] = "product description";
                        }
                        catch { }
                        int column = sheet.headers.Count();
                        int clmPlus = 2;
                        Console.WriteLine("Columns in " + sheet.name + " : " + column);
                        if(sheet.name.Equals("SKUs missing on CARiD",StringComparison.OrdinalIgnoreCase)) clmPlus++;
                        sheet.values = new string[1, column + clmPlus];
                        sheet.values[0, 0] = "Key(Brand®SKU)";
                        sheet.values[0, 1] = "Brand(your_name)";
                        if (sheet.name.Equals("SKUs missing on CARiD", StringComparison.OrdinalIgnoreCase)) sheet.values[0, 2]="Application";
                        for (int i = clmPlus; i < column + clmPlus; i++)
                        {
                            sheet.values[0, i] = sheet.headers[i - clmPlus];
                        }
                        
                        book.setValues(sheet.values, 1, column + clmPlus);                        
                        book.setColor(1, 1, 1, 2, System.Drawing.Color.DarkOliveGreen, System.Drawing.Color.White);
                        book.setColor(1, 3, 1, column + clmPlus, System.Drawing.Color.FromArgb(169, 208, 142), System.Drawing.Color.Black);
                        sheet.index = 1;
                        sheet.page = 1;
                    }
                    int rowGeneral = 1;
                    foreach (BrandView brand in brandViewList)
                    {
                        if (brand.isChecked && brand.brand.checkName!=null && !brand.brand.checkName.Equals(""))
                        {
                                main.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                               (ThreadStart)delegate ()
                               {
                                   brand.animateLoading();

                               }
                            );
                            Brand b = brand.brand;
                            book.setSheet("General");
                            rowGeneral++;
                            book.worksheet.Cells[rowGeneral, 1] = b.name;
                            book.worksheet.Cells[rowGeneral, 2] = b.checkName;
                            book.worksheet.Cells[rowGeneral, 3] = b.jobber;
                            book.worksheet.Cells[rowGeneral, 4] = b.application;
                            book.worksheet.Cells[rowGeneral, 5] = b.dateTime;
                            using (ExcelBook check = new ExcelBook())
                            {
                                try
                                {
                                    DateTime t = DateTime.Now;
                                    Console.WriteLine("Open check: " + MainWindow.pathChecks + b.checkName);
                                    check.openFile(MainWindow.pathChecks + b.checkName, true);                               

                                    foreach (Sheet sheet in sheets)
                                    {
                                        try
                                        {
                                            check.setSheet(sheet.name);
                                            Sheet sheetCheck = check.getSheet(sheet.name);
                                            if (sheetCheck == null) continue;
                                            sheetCheck.getRowsWOheaders(b.nameInCheck);
                                            int column = sheet.headers.Count();
                                            int clmPlus = 2;
                                            if (sheet.name.Equals("SKUs missing on CARiD", StringComparison.OrdinalIgnoreCase)) clmPlus++;
                                            List<Row> brandRows = sheetCheck.rows.FindAll(x => x.brand.Equals(b.nameInCheck));
                                            int row = brandRows.Count;
                                            sheetCheck.values = new object[row, column + clmPlus];
                                            Console.WriteLine("{0}:rows in: {1}", sheet.name, row);
                                            if (column == 0) continue;
                                            for (int j = 0; j < brandRows.Count; j++)
                                            {   
                                                if(!brandRows[j].sku.Equals("-"))
                                                    sheetCheck.values[j, 0] = b.name + "®" + brandRows[j].sku;
                                                else
                                                    sheetCheck.values[j, 0] = b.name + "®";
                                                sheetCheck.values[j, 1] = b.name;
                                                if (sheetCheck.name.Equals("SKUs missing on CARiD", StringComparison.OrdinalIgnoreCase)) sheetCheck.values[j, 2] = b.application;
                                            }

                                            for (int i = 0; i < sheetCheck.headers.Count; i++)
                                            {
                                                Header header = listHeaders.Find(x => x.OldName.Equals(sheetCheck.headers[i], StringComparison.OrdinalIgnoreCase) && x.Sheet.Equals(sheet.name, StringComparison.OrdinalIgnoreCase));
                                                if (header != null && !header.ignore)
                                                {
                                                    int c = sheet.headers.IndexOf(header.NewName) + clmPlus;

                                                    for (int j = 0; j < brandRows.Count; j++)
                                                    {
                                                        if (sheetCheck.values[j, c] != null && !sheetCheck.values[j, c].Equals(""))
                                                        {
                                                            if (brandRows[j].cells[i] != null && !brandRows[j].cells[i].Equals(""))
                                                                sheetCheck.values[j, c] += "|" + brandRows[j].cells[i];
                                                        }
                                                        else
                                                            sheetCheck.values[j, c] = brandRows[j].cells[i];
                                                    }

                                                }
                                            }
                                            if (sheet.page == 1)
                                                book.setSheet(sheet.name);
                                            else
                                                book.setSheet(sheet.name + "-" + sheet.page);
                                            if (sheet.index + row > 1000000)
                                            {
                                                sheet.page++;
                                                sheet.index = 1;
                                                book.addSheet(sheet.name + "-" + sheet.page);
                                                book.setSheet(sheet.name + "-" + sheet.page);

                                                book.setValues(sheet.values, 1, column + clmPlus);
                                                book.setColor(1, 1, 1, 2, System.Drawing.Color.DarkOliveGreen, System.Drawing.Color.White);
                                                book.setColor(1, 3, 1, column + clmPlus, System.Drawing.Color.FromArgb(169, 208, 142), System.Drawing.Color.Black);
                                            }

                                            book.setPartValues(sheetCheck.values, sheet.index + 1, 1, sheet.index + row, column + clmPlus);
                                            sheet.index += row;
                                        }
                                        catch (Exception ex)
                                        {
                                            Console.WriteLine("{0} - no Sheeet: {1}", b.name, sheet.name);
                                            Console.WriteLine(ex.ToString());
                                            continue;
                                        }
                                    }
                                    DateTime t2 = DateTime.Now;
                                    TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
                                    Console.WriteLine("---------------------------------------------------------------------------");
                                    Console.WriteLine("{0}:get sheet Time: {1}", b.name, elapsedSpan.TotalSeconds);
                                    Console.WriteLine("---------------------------------------------------------------------------");
                                }
                                catch (Exception ex)
                                {
                                    Console.WriteLine("Failed: " + b.name);
                                    Console.WriteLine(ex.ToString());
                                }
                            }
                        }
                        brand.isLoading = false;
                    }

                    date = DateTime.Now.ToString().Replace(' ', '_').Replace(':', '-');
                    openDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
                    if (openDialog.ShowDialog() != true)
                    {
                        Console.WriteLine("FolderError");
                        return;
                    }
                    book.saveFile(openDialog.SelectedPath + @"\One_Check_" + date + ".xlsx");
                }

                MessageBox.Show("Чек собран!");
                //if (System.IO.File.Exists(openDialog.SelectedPath + @"\One_Check_" + date + ".xlsx"))
                //{
                //    Process myProcess = new Process();
                //    myProcess.StartInfo.WorkingDirectory = MainWindow.pathChecks;
                //    myProcess.StartInfo.FileName = openDialog.SelectedPath + @"\One_Check_" + date + ".xlsx";
                //    myProcess.Start();
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Create File Error: " + ex);
                MessageBox.Show(ex.ToString());
            }
            MainWindow.isLoading = false;
        }



        private void yes_click(object sender, RoutedEventArgs e)
        {
            switch (tabContol.SelectedIndex)
            {
                case 0:
                    {
                        noBtn.Content = "Назад";
                        clear();
                        getNewHeaders();
                        foreach(CheckBox checkBox in stackPanelSheets.Children)
                        {
                            if (checkBox.IsChecked == true)
                            {
                                createSheet(checkBox.Content.ToString());
                                Console.WriteLine("Checkbox Sheet: " + checkBox.Content);
                            }
                        }
                        //if (shoudbeCheck.IsChecked == true) createSheet(shoudbeCheck.Content.ToString());
                        //if (missCheck.IsChecked == true) createSheet(missCheck.Content.ToString());
                        //if (appCheck.IsChecked == true) createSheet(appCheck.Content.ToString());
                        //if (renameCheck.IsChecked == true) createSheet(renameCheck.Content.ToString());
                        if (sheets.Count == 0) return;
                        listHeaders.Sort(delegate (Header h1, Header h2)
                        { return h1.OldName.CompareTo(h2.OldName); });

                        foreach (Header h in listHeaders)
                        {
                            if(!columns.ContainsKey(h.OldName))
                                addColumn(h);
                        }
                        tabContol.SelectedIndex = 1;
                        break;
                    }
                case 1:
                    {                  
                        saveMapping();
                        createCheckBoxes();
                        yesBtn.Content = "Собрать";
                        tabContol.SelectedIndex = 2;
                        break;
                    }
                case 2:
                    {
                        saveChecking();
                        saveColumnsToBD();
                        //createFile();
                        //createFileFromExcel();
                        
                        this.DialogResult = true;
                        break;
                    }
            }
            
        }
        private void no_click(object sender, RoutedEventArgs e)
        {
            switch (tabContol.SelectedIndex)
            {
                case 0:
                    {
                        this.DialogResult = false;
                        break;
                    }
                case 1:
                    {
                        noBtn.Content = "Отмена";
                        tabContol.SelectedIndex = 0;
                        break;
                    }
                case 2:
                    {
                        checkBoxes.Clear();
                        stackPanelCheck.Children.Clear();
                        yesBtn.Content = "Далее";
                        tabContol.SelectedIndex = 1;
                        break;
                    }
            }
            
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
