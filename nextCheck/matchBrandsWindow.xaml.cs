﻿using nextCheck.Classes;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace nextCheck
{
    /// <summary>
    /// Interaction logic for matchBrandsWindow.xaml
    /// </summary>
    public partial class matchBrandsWindow : Window
    {
        Brand brand;
        public matchBrandsWindow(Brand brand, List<string> list)
        {
            InitializeComponent();
            this.brand = brand;
            xText.Text = "В чеке "+ brand.checkName + " не найден бренд "+brand.name;
            brandListCombo.ItemsSource = list;
            brandListCombo.SelectedIndex = 0;
        }

        private void yes_click(object sender, RoutedEventArgs e)
        {
            if (brandListCombo.SelectedIndex != -1)
            {
                brand.nameInCheck = brandListCombo.SelectedItem.ToString();
                brand.error = "";
                this.DialogResult = true;
            }
        }
        private void no_click(object sender, RoutedEventArgs e)
        {
            brand.error = "В чеке отсутствует данный бренд, проверте правильность folder ID";
            this.DialogResult = false;
        }

    }
}
