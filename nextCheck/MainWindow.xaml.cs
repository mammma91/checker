﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using LiteDB;
using Microsoft.Win32;
using Newtonsoft.Json;
using nextCheck.Classes;
using nextCheck.DataBase;


namespace nextCheck
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static User user;
        public static List<Brand> arrayAllBrands;
        public static string pathApp,pathFolderDB, pathDB, pathChecks, pathFiles, pathImg;
        private static Random random;
        private DoubleAnimation loadAnimation;
        public static bool isLoading;
        Thread threadUpdateBD;
        public static bool isSearching;
        public static object locker = new object();
        public static bool checkServer = true;
        public static bool autoConfirm = true;
        public static BrandViewList brandViewList;
        public static string VERSION = "";
        public static GoogleDrive googleDrive;
        public static Client client;
        public static Config config;
        public static string selectName = "ALL";
        public Thread clientListeningThread;
        ObservableCollection<string> responsibleList;
        MainWindow mainWindow;

        public MainWindow(User user, Client client, GoogleDrive googleDrive)
        {
            InitializeComponent();
            mainWindow = this;
            MainWindow.googleDrive = googleDrive;
            googleDrive.mainWindow = this;
            MainWindow.client = client;
            MainWindow.user = user;
            try
            {
                random = new Random();
                loadAnimation = new DoubleAnimation();
                loadAnimation.Completed += ButtonAnimation_Completed;
                loadAnimation.Duration = TimeSpan.FromMilliseconds(500);
                loadAnimation.AutoReverse = true;

                loadAnimation.FillBehavior = FillBehavior.HoldEnd;
                isLoading = false;
                isSearching = false;
                pathApp = Environment.CurrentDirectory;
                pathDB = pathApp + @"\db\MyData.db";
                pathFolderDB = pathApp + @"\db\";
                pathFiles = pathApp + @"\files\";
                pathChecks = pathFiles + @"checks\";
                pathImg = pathFiles + @"images\";

                if (!File.Exists(pathFolderDB + "allMissing.db"))
                {
                    Console.WriteLine("******************************************-DELETE DB");
                    string[] filePaths = Directory.GetFiles(pathApp + @"\db", ".", SearchOption.AllDirectories);
                    foreach (string file in filePaths)
                    {
                        System.IO.File.Delete(file);
                        Console.WriteLine("Delete: " + file);
                    }
                }

                window.Icon = new BitmapImage(new Uri(pathImg + @"checker2.png"));
                mainBackground.ImageSource = new BitmapImage(new Uri(pathImg + @"background_main.jpg"));
                logoImg.ImageSource = new BitmapImage(new Uri(pathImg + @"checker2.png"));
                xDeleteBtn.MouseMove += XDeleteBtn_MouseMove;
                xDeleteBtn.MouseLeave += XDeleteBtn_MouseLeave;
                searchBox.TextChanged += SearchBox_TextChanged;
                sortBox.SelectionChanged += SortBox_SelectionChanged;
                datePicker1.SelectedDateChanged += DatePicker_SelectedDateChanged;
                datePicker2.SelectedDateChanged += DatePicker2_SelectedDateChanged;
                hotCall.Click += HotCall_Click;
                manual.Click += Manual_Click;
                offer.Click += Offer_Click;
                offer.Visibility = Visibility.Hidden;
                xGetFolderID.Click += XGetFolderID_Click;
                xGetFolderID.Visibility = Visibility.Hidden;
                updateBDbtn.Visibility = Visibility.Hidden;
                checkBoxConnectToServer.Checked += CheckBoxConnectToServer_Checked;
                checkBoxConnectToServer.Unchecked += CheckBoxConnectToServer_Unchecked;
                checkBoxAutoFalse.Checked += CheckBoxAutoFalse_Checked;
                checkBoxAutoFalse.Unchecked += CheckBoxAutoFalse_Unchecked;
                responsibleList = new ObservableCollection<string>();
                updateResponsibleList();
                responsibleListCombo.ItemsSource = responsibleList;
                responsibleListCombo.SelectedItem = selectName;
                responsibleListCombo.SelectionChanged += ResponsibleListCombo_SelectionChanged;
                config = new Config();
                if (config.getConfigFromFile()) {
                    versionLabel.Content = "version - " + config.version;
                }

                TextBlockUsername.Text = user.mail;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            
            using (LiteDatabase db = new LiteDatabase(pathDB))
            {
                var col = db.GetCollection<DataBase.Brand>("brands");
                Console.WriteLine("База данных содержит"+col.Count()+"брендов");
                addLog("База данных содержит" + col.Count() + "брендов");
            }
            
            brandViewList = new BrandViewList(this);
            //animateLoading();
            brandViewList.addBrandsToView();

            this.Loaded += MainWindow_Loaded;
            Closed += MainWindow_Closed;
            clientListeningThread = new Thread(new ThreadStart(ClientListening));
            clientListeningThread.Start();
            
        }

        private void ResponsibleListCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Console.WriteLine("Chage Select");
            using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "importers.db"))
            {
                var colSheet = dbsheet.GetCollection<Importer>("Importer");
                colSheet.EnsureIndex(x => x.select);
                Importer importer = colSheet.FindOne(x => x.select==true);
                importer.select = false;
                colSheet.Update(importer);
                colSheet.EnsureIndex(x => x.name);
                ComboBox box = (ComboBox)sender;
                importer = colSheet.FindOne(x => x.name.Equals(box.SelectedItem.ToString(), StringComparison.OrdinalIgnoreCase));
                selectName = importer.name;
                importer.select = true;
                colSheet.Update(importer);
            }
            brandViewList.updateCountSku();
        }

        public void updateResponsibleList()
        {
            
            using (LiteDatabase dbsheet = new LiteDatabase(MainWindow.pathFolderDB + "importers.db"))
            {
                var colSheet = dbsheet.GetCollection<Importer>("Importer");
                colSheet.EnsureIndex(x => x.name);
                if (!colSheet.Exists(x => x.name.Equals("ALL")))
                {
                    colSheet.Insert(new Importer() { name = "ALL", select = true });
                }
                List<Importer> importers = colSheet.FindAll().ToList();
                importers.Sort(delegate (Importer im1, Importer im2) { return im1.name.CompareTo(im2.name); });             
                foreach (Importer importer in importers)
                {
                    if (!responsibleList.Contains(importer.name))
                    {
                        responsibleList.Add(importer.name);
                        if (importer.select) selectName = importer.name;
                    }
                }
                
            }
        }

        private void DatePicker2_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
              brandViewList.selectByDate();
        }

        bool listen = true;

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            listen = false;
            Console.WriteLine("window closed");
            Client.SendMessage("close",null);
            Client.Disconnect();
            //notifier.Dispose();
        }
      
        public void ClientListening()
        {
            while (listen)
            {
                if(client==null || !client.getStatus())
                {
                    Console.WriteLine("Try to start connection");
                    client = new Client(user);
                    if (client.start())
                    {                       
                        if (client.checkVersion())
                        {
                            Client.StartUpdater();
                            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)delegate ()
                            {
                                System.Windows.Application.Current.Shutdown();
                            }
                                     );
                            Thread.CurrentThread.Abort();
                        }
                        client.startListening();
                        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                serverStatus.Text = "online";
                                serverStatus.Background = new SolidColorBrush() { Color = Colors.DarkGreen, Opacity = 0.7 };
                            }
                        );
                        
                    }
                    else
                    {
                        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                            (ThreadStart)delegate ()
                            {
                                serverStatus.Text = "offline";
                                serverStatus.Background = new SolidColorBrush() { Color = Colors.DarkRed, Opacity = 0.7 };
                            }
                        );
                        
                    }
                }else
                if (DateTimeOffset.UtcNow.ToUnixTimeSeconds() - client.unixTime > 30)
                {
                    Console.WriteLine("Send: echo");
                    Client.SendMessage("echo",null);
                    client.unixTime = DateTimeOffset.UtcNow.ToUnixTimeSeconds();
                }
                //Console.WriteLine("Listening client");
                Thread.Sleep(3000);
            }
        }

        private void CreateOneCheck(object sender, RoutedEventArgs e)
        {
            brandViewList.updateBrandList();
            CreateCheckWindow createCheckWindow = new CreateCheckWindow(this);
            if (createCheckWindow.ShowDialog() == true)
            {
                animateLoading();
                Thread create = new Thread(() => createCheckWindow.createFileFromExcel());
                create.Start();

            }

            //Ookii.Dialogs.Wpf.VistaFolderBrowserDialog openDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            //if (openDialog.ShowDialog() != true)
            //{
            //    addLog("FolderError");
            //    return;
            //}
            //animateLoading();
            //Thread getInfo = new Thread(() => saveSheetsToFile());
            //getInfo.Start();
        }

        private void saveSheetsToFile()
        {
            try
            {
                DateTime t = DateTime.Now;
                List<Row> list = new List<Row>();

                using (LiteDatabase db = new LiteDatabase(pathFolderDB + "Application".Replace(" ", "") + ".db"))
                {
                    var colSheet = db.GetCollection<DataBase.Row>("sheet");
                    colSheet.EnsureIndex(x => x.Id);
                    int k = colSheet.Count();
                    for (int i = 1; i < k; i++)
                    {
                        Row r = colSheet.FindOne(x => x.Id==i);
                        list.Add(r);
                        r = null;
                    }
                    //foreach (BrandView brandView in brandViewList.brandList)
                    //{
                    //    if (brandView.isChecked && colSheet.Exists(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)))
                    //    {
                    //for (int i = 0; i < k; i++)
                    //{
                    //    Row r = colSheet.FindOne(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase));
                    //    list.Add(r);
                    //    r = null;
                    //}


                    //    }
                    //}
                    //colSheet.EnsureIndex(x => x.sku);
                    //list.AddRange(colSheet.Find(x => x.sku.Equals("HEADER", StringComparison.OrdinalIgnoreCase)).ToList());
                    //foreach (BrandView brandView in brandViewList.brandList)
                    //{
                    //    if (brandView.isChecked) //&& colSheet.Exists(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)))

                    //}
                    //foreach (BrandView brandView in brandViewList.brandList)
                    //{
                    //    if (brandView.isChecked && colSheet.Exists(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)))
                    //    {
                    //        List<Row> rows = colSheet.Find(x => x.brand.Equals(brandView.brand.nameInCheck, StringComparison.OrdinalIgnoreCase)).Where(x => !x.sku.Equals("HEADER")).ToList();
                    //        foreach (Row row in rows)
                    //        {
                    //            row.brandNameFromFile = brandView.brand.name;
                    //        }
                    //        list.AddRange(rows);
                    //        rows.Clear();
                    //        rows = null;
                    //    }
                    //}
                }
                DateTime t2 = DateTime.Now;
                TimeSpan elapsedSpan = new TimeSpan(t2.Ticks - t.Ticks);
                Console.WriteLine("Get sheet Time: " + (elapsedSpan.TotalSeconds));
                Console.WriteLine("List: " + list.Count);
                list.Clear();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Ex: " + ex);
            }
        }


        private void CheckBoxAutoFalse_Unchecked(object sender, RoutedEventArgs e)
        {
            autoConfirm = false;
        }
        private void CheckBoxAutoFalse_Checked(object sender, RoutedEventArgs e)
        {
            autoConfirm = true;
        }
        private void CheckBoxConnectToServer_Unchecked(object sender, RoutedEventArgs e)
        {
            checkServer = false;
        }
        private void CheckBoxConnectToServer_Checked(object sender, RoutedEventArgs e)
        {
            checkServer = true;
        }       
        private void XGetFolderID_Click(object sender, RoutedEventArgs e)
        {
            //Thread serchParentThread = new Thread(searchParentFromFile);
            //serchParentThread.Start();          
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (System.IO.File.Exists(pathFiles + @"\version.txt"))
            {
                try
                {
                    string lines = File.ReadAllText(pathFiles + @"\version.txt");
                    MessageBox.Show(lines);
                    System.IO.File.Delete(pathFiles + @"\version.txt");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }
        private void Offer_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("https://drive.google.com/open?id=17KDBkcIiIpRTDJF-osnCiBH3yp2MHXOcKL9G1GnaZ70");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        private void Manual_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("https://drive.google.com/open?id=1uVsMPyXticI5MHLwr6EJj_FZilnIN03xMC0oj_hKrLA");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        private void HotCall_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("https://join.skype.com/invite/lMRim7QDfim6");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void DatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            brandViewList.selectByDate();
        }
        private void SortBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            brandViewList.sort();
        }
        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            brandViewList.searchInView(sender);
        }


        private void XDeleteBtn_MouseLeave(object sender, MouseEventArgs e)
        {
            xDeleteBtn.Background = new SolidColorBrush() { Color = Colors.OrangeRed, Opacity = 0.4 };
        }
        private void XDeleteBtn_MouseMove(object sender, MouseEventArgs e)
        {
            xDeleteBtn.Background = new SolidColorBrush() { Color = Colors.OrangeRed, Opacity = 0.3 };
        }

        private void addBrandBtn_click(object sender, RoutedEventArgs e)
        {
            brandViewList.addBrandToView();
        }

        private void V_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            BrandView v = sender as BrandView;
            BrandSettingWindow brandSettingWindow = new BrandSettingWindow(v.brand);
            if (brandSettingWindow.ShowDialog() == true)
            {
                v.check(v.isChecked);
            }
        }

        private void loadListBrand_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Файлы excel|*.xlsx";
            if (openDialog.ShowDialog() != true)
            {
                addLog("error");
                return;
            }
            animateLoading();
            Thread getBrands = new Thread(() => getBrandsFromExcel(openDialog.FileName));
            getBrands.Start();

           
        }

        private void searchBrand_Click(object sender, RoutedEventArgs e)
        {
            if (!isSearching)
            {
                serachBtn.Content = "Стоп";
                isSearching = true;
                animateLoading();
                brandViewList.updateBrandList();
                Thread myThread = new Thread(new ThreadStart(brandViewList.search));
                myThread.Start();
            }
            else
            {
                isSearching = false;
                serachBtn.IsEnabled = false;
            }
        }

        private void deleteBtn_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.deleteFromView();
        }
        private void unCheckAll_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.uncheckALL();
        }
        private void checkAll_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.checkAll();
        }
        private void checkAllEmpty_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.checkAllEmpty();
        }
        private void checkAllRotten_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.checkAllRotten();
        }
        private void updateBDbtn_click(object sender, RoutedEventArgs e)
        {
        }
        private void getАllInfo_click(object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaFolderBrowserDialog openDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (openDialog.ShowDialog() != true)
            {
                addLog("FolderError");
                return;
            }
            animateLoading();
            Thread getInfo = new Thread(() => saveAllInfoToFile(openDialog.SelectedPath));
            getInfo.Start();
            //Thread getInfo = new Thread(() => saveAllInfoFromBDToFile(openDialog.SelectedPath));
            //getInfo.Start();
        }

        private void saveAllInfoFromBDToFile(string path)
        {
            string date = "";
            List<Brand> list = new List<Brand>();
            using (LiteDatabase db = new LiteDatabase(pathApp + @"\MyData.db"))
            {
                var col = db.GetCollection<DataBase.Brand>("brands");
                list = col.FindAll().ToList();
            }
            using (ExcelBook book = new ExcelBook())
            {
                book.createFile();
                book.addSheet("Brands");
                book.setSheet("Brands");
                string[,] array = new string[list.Count + 2, 2];
                array[0, 0] = "Brand";

                array[0, 1] = "FolderID";


                for (int i = 1; i < list.Count + 1; i++)
                {
                    array[i, 0] = list[i - 1].name;
                    array[i, 1] = list[i - 1].folderID;

                }

                book.setValues(array, list.Count + 1, 2);
                date = DateTime.Now.ToString().Replace(' ', '_').Replace(':', '-');
                book.saveFile(path + @"\info_Brands_" + date + ".xlsx");

            }
            isLoading = false;
            if (System.IO.File.Exists(path + @"\info_Brands_" + date + ".xlsx"))
            {
                Process myProcess = new Process();
                myProcess.StartInfo.WorkingDirectory = MainWindow.pathChecks;
                myProcess.StartInfo.FileName = path + @"\info_Brands_" + date + ".xlsx";
                myProcess.Start();
            }
        }

        private void saveAllInfoToFile(string path)
        {
            string date = "";
            List<Brand> list = new List<Brand>();
            using (LiteDatabase db = new LiteDatabase(pathDB))
            {
                var col = db.GetCollection<DataBase.Brand>("brands");
                list = col.FindAll().ToList();
            }
            using (ExcelBook book = new ExcelBook())
            {
                book.createFile();
                book.addSheet("Brands");
                book.setSheet("Brands");
                string[,] array = new string[list.Count + 2, 13];
                array[0, 0] = "Brand";
                array[0, 1] = "NameInCheck";
                array[0, 2] = "BrandID";
                array[0, 3] = "isChecking";
                array[0, 4] = "FolderID";
                array[0, 5] = "CheckName";
                array[0, 6] = "Date";
                array[0, 7] = "Owner";
                array[0, 8] = "Link";
                array[0, 9] = "Jobber";
                array[0, 10] = "Application";
                array[0, 11] = "Error";
                array[0, 12] = "LastUpdateTime";

                for (int i = 1; i < list.Count + 1; i++)
                {
                    array[i, 0] = list[i - 1].name;
                    array[i, 1] = list[i - 1].nameInCheck;
                    array[i, 2] = list[i - 1].brandid;
                    array[i, 3] = list[i - 1].isChecking ? "Y" : "N";
                    array[i, 4] = list[i - 1].folderID;
                    array[i, 5] = list[i - 1].checkName;
                    array[i, 6] = list[i - 1].dateTime;
                    array[i, 7] = list[i - 1].owner;
                    array[i, 8] = list[i - 1].link;
                    array[i, 9] = list[i - 1].jobber;
                    array[i, 10] = list[i - 1].application;
                    array[i, 11] = list[i - 1].error;
                    //array[i, 12] = list[i - 1].dateTimeLastUpdate;
                }

                book.setValues(array, list.Count + 1, 13);
                date = DateTime.Now.ToString().Replace(' ', '_').Replace(':', '-');
                book.saveFile(path + @"\info_Brands_" + date + ".xlsx");
               
            }
            isLoading = false;
            if (System.IO.File.Exists(path + @"\info_Brands_" + date + ".xlsx"))
            {
                Process myProcess = new Process();
                myProcess.StartInfo.WorkingDirectory = MainWindow.pathChecks;
                myProcess.StartInfo.FileName = path + @"\info_Brands_" + date + ".xlsx";
                myProcess.Start();
            }
        }

        private void getChecksToFolder(string path, List<BrandView> list)
        {

            foreach (BrandView br in list)
            {
                if (br.isChecked && br.brand.checkName != null)
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                           (ThreadStart)delegate ()
                           {
                               br.animateLoading();
                           }
                        );
                    try
                    {
                        if (!System.IO.File.Exists(path + @"\" + br.brand.checkName) && System.IO.File.Exists(MainWindow.pathChecks + br.brand.checkName))
                        {
                            System.IO.File.Copy(MainWindow.pathChecks + br.brand.checkName, path + @"\" + br.brand.checkName, true);
                        }
                        else
                        {
                            addLog("The following file: \"" + br.brand.checkName + "\" already exists.");
                        }
                    }
                    catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                           (ThreadStart)delegate ()
                           {
                               br.isLoading = false;
                           }
                        );

                }
            }
            isLoading = false;
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                           (ThreadStart)delegate ()
                           {
                               MessageBox.Show("Done!");
                           }
                        );
        }

        private void getChecks_click(object sender, RoutedEventArgs e)
        {
            Ookii.Dialogs.Wpf.VistaFolderBrowserDialog openDialog = new Ookii.Dialogs.Wpf.VistaFolderBrowserDialog();
            if (openDialog.ShowDialog() != true)
            {
                addLog("FolderError");
                return;
            }

            animateLoading();
            List<BrandView> list = new List<BrandView>();
            foreach (BrandView br in xUpdateChecks.Children)
            {
                if (br.isChecked)
                    list.Add(br);
            }
            Thread getChecks = new Thread(() => getChecksToFolder(openDialog.SelectedPath, list));
            getChecks.Start();

        }


        public void animateLoading()
        {
            isLoading = true;
            loadAnimation.From = 0;
            loadAnimation.To = (int)updateBDbtn.ActualWidth;
            loadLabel.BeginAnimation(Button.WidthProperty, loadAnimation);
        }

        private void ButtonAnimation_Completed(object sender, EventArgs e)
        {
            if (isLoading) { animateLoading(); }
        }

        private void mainGrid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void rotten1_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.selectByRotten(60);
        }

        private void rotten6_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.selectByRotten(6*60);
        }

        private void rotten12_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.selectByRotten(12*60);
        }

        private void rotten24_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.selectByRotten(24*60);
        }

        private void menuDate_Click(object sender, RoutedEventArgs e)
        {

        }
        
        private void error_Click(object sender, RoutedEventArgs e)
        {
            brandViewList.selectByError();
        }

        public void openMatchBrandsWindow(Brand brand, List<string> brandsInCheck)
        {
            DispatcherOperation op = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                      (ThreadStart)delegate ()
                      {
                          matchBrandsWindow brandSettingWindow = new matchBrandsWindow(brand, brandsInCheck);
                          if (brandSettingWindow.ShowDialog() == false)
                          {
                              addLog("not found " + brand.name + " in check" + brand.checkName);
                          }
                      }
                  );
            op.Wait();
        }

        private void getBrandsFromExcel(string path)
        {
            //---All brand isChecking -> false
            using (LiteDatabase db = new LiteDatabase(pathDB))
            {
                var col = db.GetCollection<DataBase.Brand>("brands");               
                var res = col.Find(x => x.isChecking == true);
                if (res != null)
                {
                    foreach (Brand brand in res)
                    {
                        if (brand.isChecking)
                        {
                            brand.isChecking = false;
                            col.Update(brand);
                        }
                    }
                }
            }

            List<string> listOfNames = new List<string>();
            try
            {
                using (ExcelBook excelBook = new ExcelBook())
                {
                    addLog("Loading: " + path);
                    excelBook.openFile(path, true);

                    object[,] allValueFromBook = excelBook.getAllValues();
                    int sizeCol = excelBook.lastCell.Column;
                    int sizeRow = excelBook.lastCell.Row;
                    List<string> clmnNames = new List<string>();
                    for (int i = 1; i <= sizeCol; i++)
                    {
                        if (allValueFromBook[1, i] != null)
                        {
                            clmnNames.Add(allValueFromBook[1, i].ToString());
                        }
                    }
                    if (!clmnNames.Contains("Brand")) throw new Exception("Не найдена колонка Brand");
                    if (clmnNames.Contains("FolderID"))
                    {
                        addLog("----FolderID is----");
                    }
                    using (LiteDatabase db = new LiteDatabase(pathDB))
                    {
                        var col = db.GetCollection<DataBase.Brand>("brands");
                        List<Brand> list = col.FindAll().ToList();
                        List<Brand> listToADD = new List<Brand>();
                        for (int i = 2; i <= sizeRow; i++)
                        {                       
                            if (allValueFromBook[i, clmnNames.IndexOf("Brand") + 1] != null)
                            {
                                Brand brand = null;
                                string name = allValueFromBook[i, clmnNames.IndexOf("Brand") + 1].ToString();
                                int index = name.IndexOf("®");
                                if (index != -1)
                                    name = name.Remove(index, 1);
                                
                                if (!listOfNames.Contains(name))
                                {                                    
                                    listOfNames.Add(name);
                                    brand = list.Find(x=>x.name.Equals(name,StringComparison.OrdinalIgnoreCase));//col.FindOne((x => x.name.Equals(name)));                                   
                                    if (brand != null)
                                    {
                                        if (brand.name.Equals(name))
                                        {
                                            Console.WriteLine("Найден бренд: " + name);
                                        }
                                        else
                                        {
                                            Console.WriteLine("Найден бренд, но с другим регистром: " + name +" - "+ brand.name);
                                            brand.name = name;
                                        }
                                        brand.isChecking = true;                                      
                                        if (brand.folderID == null || brand.folderID.Equals(""))
                                        {
                                            if (clmnNames.Contains("FolderID") && allValueFromBook[i, clmnNames.IndexOf("FolderID") + 1] != null)
                                            {
                                                brand.folderID = allValueFromBook[i, clmnNames.IndexOf("FolderID") + 1].ToString();
                                            }
                                        }
                                        col.Update(brand);
                                    }
                                    else
                                    {
                                        Brand tempBrand = new Brand();
                                        Console.WriteLine("Не найден бренд: " + name);                                                                                 
                                        tempBrand.name = name;
                                        tempBrand.isChecking = true;
                                        if (clmnNames.Contains("FolderID") && allValueFromBook[i, clmnNames.IndexOf("FolderID") + 1] != null)
                                        {
                                            tempBrand.folderID = allValueFromBook[i, clmnNames.IndexOf("FolderID") + 1].ToString();
                                        }
                                        
                                        list.Add(tempBrand);
                                        listToADD.Add(tempBrand);
                                    }

                                }
                            }                           
                        }
                        col.InsertBulk(listToADD);
                    }
                    addLog("Successful", Colors.DarkGreen);
                }               
            }
            catch (Exception ex)
            {
                addLog(ex.ToString(), Colors.DarkRed);
            }      
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
              (ThreadStart)delegate ()
              {
                  brandViewList.addBrandsToView();
                 // brandViewList.checkBrandsFromServer();
              }
            );
            
            isLoading = false;
        }

        public void addLog(string str)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
              (ThreadStart)delegate ()
              {
                  listLogo.Items.Add(new ListBoxItem().Content = str);
                  listLogo.ScrollIntoView(listLogo.Items[listLogo.Items.Count - 1]);
              }
           );

        }
        public void addLog(string str, Color color)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                (ThreadStart)delegate ()
                {
                    ListBoxItem item = new ListBoxItem();
                    item.Content = str;
                    item.Background = new SolidColorBrush(color);
                    listLogo.Items.Add(item);
                    listLogo.ScrollIntoView(listLogo.Items[listLogo.Items.Count - 1]);
                }
           );
        }

        //public static Notifier notifier = new Notifier(cfg =>
        //{
        //    cfg.PositionProvider = new PrimaryScreenPositionProvider(Corner.BottomRight, 10, 30);
        //    cfg.LifetimeSupervisor = new TimeAndCountBasedLifetimeSupervisor(
        //        notificationLifetime: TimeSpan.FromSeconds(60*60*24),
        //        maximumNotificationCount: MaximumNotificationCount.FromCount(3));

        //    cfg.Dispatcher = Application.Current.Dispatcher;
        //    cfg.DisplayOptions.TopMost = true;
        //});

    }
}

