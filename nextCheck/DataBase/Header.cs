﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nextCheck.DataBase
{
    public class Header
    {
        public int Id { get; set; }
        public string Sheet { get; set; }
        public string OldName { get; set; }
        public string NewName { get; set; }
        public bool ignore { get; set; }
    }
}
