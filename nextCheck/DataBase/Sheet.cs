﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nextCheck.DataBase
{
    public class Sheet
    {
        public int Id { get; set; }
        public string name { get; set; }
        public List<string> headers { get; set; }
        public List<string> brands { get; set; }
        public List<Row> rows { get; set; }
        public List<Row> headersDB { get; set; }
        public object[,] values { get; set; }
        public int index;
        public int page;
        public int idSKU =-1;

        public void setIdSKU()
        {
            if (values != null)
            {
                idSKU = headers.IndexOf("sku");
            }
        }

        public Row getValuesFromRow(int row, string name)
        {
            List<string> list = new List<string>();
            string cellSKU = "-";
            int column = values.GetUpperBound(1);
            for (int j = 1; j <= column; j++)
            {
                if (values[row, j] == null) list.Add("");
                else
                if (row == 1)
                    list.Add("'"+values[row, j].ToString().ToLower());
                else
                    list.Add("'"+values[row, j].ToString());
            }
            if (row == 1) cellSKU = "HEADER";
            else
            if (idSKU != -1 && values[row, idSKU + 1]!=null) cellSKU = values[row, idSKU + 1].ToString();
            return new Row { cells = list, sku = cellSKU, brand = name };
        }

        public void setValuesFromRows(List<Row> rows)
        {

        }

        public void setHeadersFromRows(List<Row> rows)
        {
            headers = new List<string>();
            foreach(Row r in rows)
            {
                foreach (string h in r.cells)
                {
                    if (!headers.Contains(h))
                    {
                        headers.Add(h);
                    }
                }
            }
        }

        //public Row getValuesFromRow(int row, string name)
        //{
        //    string list = "";
        //    string cellSKU = "";
        //    int column = values.GetUpperBound(1);
        //    for (int j = 1; j <= column; j++)
        //    {
        //        if (values[row, j] == null) list+="|";
        //        else
        //            list += values[row, j].ToString()+"|";
        //    }
        //    if (row == 1)
        //    {
        //        cellSKU = "HEADER";
        //        list=list.ToLower();
        //    }
        //    else
        //    if (idSKU != -1) cellSKU = values[row, idSKU + 1].ToString();
        //    return new Row { cells = list, sku = cellSKU, brand = name };
        //}


        public List<Row> getRows(string brand)
        {
            int brandColumn = headers.IndexOf("brand");
            Console.WriteLine("brandColumn: " + brandColumn);
            setIdSKU();
            List<Row> list = new List<Row>();
            int rows = values.GetUpperBound(0);
            //Console.WriteLine("rows: " + rows);
            //Console.WriteLine("columns: " + values.GetUpperBound(1));
            for (int i = 1; i <= rows; i++)
            {
                if (brandColumn==-1 || i==1)
                    list.Add(getValuesFromRow(i,brand));
                else
                if (values[i, brandColumn+1] != null && brand.Equals(values[i, brandColumn+1].ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    list.Add(getValuesFromRow(i,brand));
                }
            }
            this.rows = list;
            return list;
        }

        public List<Row> getRowsWOheaders(string brand)
        {
            int brandColumn = headers.IndexOf("brand");
            Console.WriteLine("brandColumn: " + brandColumn);
            setIdSKU();
            List<Row> list = new List<Row>();
            int rows = values.GetUpperBound(0);
            //Console.WriteLine("rows: " + rows);
            //Console.WriteLine("columns: " + values.GetUpperBound(1));
            for (int i = 2; i <= rows; i++)
            {
                if (brandColumn == -1)
                    list.Add(getValuesFromRow(i, brand));
                else
                if (values[i, brandColumn + 1] != null && brand.Equals(values[i, brandColumn + 1].ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    list.Add(getValuesFromRow(i, brand));
                }
            }
            this.rows = list;
            return list;
        }

        //public List<Row> getRows(string brand, List<Row> except)
        //{
        //    int brandColumn = headers.IndexOf("brand");
        //    Console.WriteLine("brandColumn: " + brandColumn);
        //    setIdSKU();
        //    List<Row> list = new List<Row>();
        //    Row r;
        //    int rows = values.GetUpperBound(0);
        //    //Console.WriteLine("rows: " + rows);
        //    //Console.WriteLine("columns: " + values.GetUpperBound(1));
        //    for (int i = 1; i <= rows; i++)
        //    {
        //        if(i == 1)
        //            list.Add(getValuesFromRow(i, brand));
        //        else
        //        if (brandColumn == -1)
        //        {
        //            r = getValuesFromRow(i, brand);
        //            foreach (Row row in except)
        //            {
        //                if (row.sku.Equals(r.sku, StringComparison.OrdinalIgnoreCase))
        //                {
        //                    list.Add(r);
        //                    break;
        //                }
        //            }                  
        //        }
        //        else
        //        if (values[i, brandColumn + 1] != null && brand.Equals(values[i, brandColumn + 1].ToString(), StringComparison.OrdinalIgnoreCase))
        //        {
        //            r = getValuesFromRow(i, brand);
        //            foreach (Row row in except)
        //            {
        //                if (row.sku.Equals(r.sku, StringComparison.OrdinalIgnoreCase))
        //                {
        //                    list.Add(r);
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    this.rows = list;
        //    return list;
        //}
    }
}
