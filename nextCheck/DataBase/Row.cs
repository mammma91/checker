﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nextCheck.DataBase
{
    public class Row
    {
        public int Id { get; set; }
        public string sku { get; set; }
        public string brand { get; set; }
        public string brandNameFromFile { get; set; }
        public List<string> cells { get; set; }
        //public string cells { get; set; }
    }
}
