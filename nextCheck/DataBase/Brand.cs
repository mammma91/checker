﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Threading.Tasks;

namespace nextCheck.DataBase
{
    public class Brand
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string name { get; set; }
        public string nameInCheck { get; set; }
        public string brandid { get; set; }
        public string folderID { get; set; }
        public string checkID { get; set; }
        public string checkName { get; set; }
        public long time { get; set; }
        [JsonIgnore]
        public string dateTime { get; set; }
        [JsonIgnore]
        public string owner { get; set; }
        [JsonIgnore]
        public string link { get; set; }
        [JsonIgnore]
        public string error { get; set; }
        [JsonIgnore]
        public long timeCreate { get; set; }     
        [JsonIgnore]
        public long timeLastUpdate { get; set; }
        [JsonIgnore]
        public string dateTimeLastUpdate { get; set; }
        [JsonIgnore]
        public int id { get; set; }
        [JsonIgnore]
        public bool isChecking { get; set; }
        [JsonIgnore]
        public string jobber { get; set; }
        [JsonIgnore]
        public string application { get; set; }
        //[JsonIgnore]
        //public Dictionary<string,List<Row>> sheets { get; set; }

        public void set(Brand br)
        {
            name = br.name;
            nameInCheck = br.nameInCheck;
            brandid = br.brandid;
            folderID = br.folderID;
            checkID = br.checkID;
            checkName = br.checkName;
            dateTime = br.dateTime;
            link = br.link;
            error = br.error;
            timeCreate = br.timeCreate;
            time = br.time;
            isChecking = br.isChecking;
            jobber = br.jobber;
            application = br.application;
            owner = br.owner;
            //sheets = br.sheets;
        }
    }
}
