﻿using nextCheck.Classes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using WpfAnimatedGif;

namespace nextCheck
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {

        GoogleDrive googleDrive;
        Client client;
        User user;

        public LoginWindow()
        {
            InitializeComponent();
            string updaterDirectory = Environment.CurrentDirectory;
            var image = new BitmapImage();
            image.BeginInit();
            image.UriSource = new Uri(updaterDirectory + @"\files\images\loading.gif");
            image.EndInit();

            ImageBehavior.SetRepeatBehavior(waitGif, RepeatBehavior.Forever);
            ImageBehavior.SetAnimatedSource(waitGif, image);
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            //logoImg.ImageSource = new BitmapImage(new Uri(@"D:\Work\Nexteum\programms\projectChecker\checker\nextCheck\Assets\sign_2.png"));
            logBox.Text = "Запрос доступа...";
            button.Visibility = Visibility.Hidden;
            button.Height = 0;
            waitGif.Height = 60;
            logBox.Visibility = Visibility.Visible;
            logBox.Height = 20;

            Thread threadupdatebd = new Thread(LoginGoogle);
            threadupdatebd.Start();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            Button btn = (Button)sender;
            btn.Background.Opacity = 0.6;
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            Button btn = (Button)sender;
            btn.Background.Opacity = 1;
        }

        private void LoginGoogle()
        {
            googleDrive = new GoogleDrive();
            if (googleDrive.AuthenticateOauth())
            {
                user = new User(googleDrive.user,googleDrive.mail,googleDrive.token);
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
              (ThreadStart)delegate ()
              {
                  logBox.Text = "Подключение к серверу...";
              }
              );
                Thread.Sleep(1000);
                new Thread(LoginServer).Start();              
            }
            else
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
               (ThreadStart)delegate ()
               {
                   logBox.Text = "Ошибка доступа Google";                 
               }
               );
                Thread.Sleep(1000);
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
              (ThreadStart)delegate ()
              {
                  logBox.Text = "";
                  button.Visibility = Visibility.Visible;
                  button.Height = 42;
                  waitGif.Height = 0;
                  logBox.Visibility = Visibility.Hidden;
                  logBox.Height = 0;
              }
              );
            }

        }


        private void LoginServer()
        {
            client = new Client(user);
            if (client.start())
            {           
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,(ThreadStart)delegate ()
                 {
                     logBox.Text = "Проверка версии...";
                 }
                 );
                if (client.checkVersion())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,(ThreadStart)delegate()
                    {
                        logBox.Text = "Обновление...";
                    }
                    );
                    Thread.Sleep(1000);
                    Client.StartUpdater();
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,(ThreadStart)delegate ()
                             {
                                 System.Windows.Application.Current.Shutdown();
                             }
                             );
                    Thread.CurrentThread.Abort();
                }
                client.startListening();
            }
            else
            {
                this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
              (ThreadStart)delegate ()
              {
                  logBox.Text = "Ошибка доступа";
              }
              );
               
            }
            Thread.Sleep(1000);
            this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
             (ThreadStart)delegate ()
             {
                    //System.Windows.Application.Current.Shutdown();
                 logBox.Text = "Запуск...";
                 MainWindow mainWindow = new MainWindow(user,client,googleDrive);
                 mainWindow.Show();
                 this.Close();
             }
             );
        }
    }
}
