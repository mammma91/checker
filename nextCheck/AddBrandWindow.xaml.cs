﻿using LiteDB;
using nextCheck.Classes;
using nextCheck.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace nextCheck
{
    /// <summary>
    /// Interaction logic for AddBrandWindow.xaml
    /// </summary>
    public partial class AddBrandWindow : Window
    {
        private Brand brand, tempBrand;
        private List<Brand> array;
        private List<string> names;
        public AddBrandWindow(Brand brand)
        {
            InitializeComponent();
            this.brand = brand;
            names = new List<string>();

            //using (ApplicationContext db = new ApplicationContext())
            //{
            //    array = db.Brands.ToList();
            //}
            using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
            {
                array = db.GetCollection<DataBase.Brand>("brands").FindAll().ToList();              
            }
            foreach (Brand b in array)
            {
                names.Add(b.name);
            }

            brandListCombo.ItemsSource = names;
            brandListCombo.KeyUp += BrandListCombo_KeyUp;

            brandIdBox.IsEnabled = false;
            folderIdBox.IsEnabled = false;
        }



        private void BrandListCombo_KeyUp(object sender, KeyEventArgs e)
        {     
            if (Keyboard.IsKeyDown(Key.Back))
            {
                if (tempBrand != null)
                {
                    tempBrand = null;
                    brandIdBox.Text = "";
                    folderIdBox.Text = "";
                }            
            }
            if (names.Contains(brandListCombo.Text))
            {
                tempBrand = array[names.IndexOf(brandListCombo.Text)];
                if (tempBrand.brandid != null)
                {
                    brandIdBox.Text = tempBrand.brandid;
                }
                else
                {
                    brandIdBox.Text = "";
                }
                if (tempBrand.folderID != null)
                {
                    folderIdBox.Text = tempBrand.folderID;
                }
                else
                {
                    folderIdBox.Text = "";
                }
            }
            else
            {
                if (tempBrand != null)
                {
                    tempBrand = null;
                    brandIdBox.Text = "";
                    folderIdBox.Text = "";
                }

            }
        }


        private void yes_click(object sender, RoutedEventArgs e)
        {
            
            if (brandListCombo.Text != "")
            {
                using (LiteDatabase db = new LiteDatabase(MainWindow.pathDB))
                {
                    var col = db.GetCollection<DataBase.Brand>("brands");
          
                    if (tempBrand != null)
                    {
                        tempBrand = col.FindOne(x => x.name.Equals(tempBrand.name));                       
                        tempBrand.isChecking = true;
                        col.Update(tempBrand);
                        brand.set(tempBrand);
                        //db.Brands.Update(brand);
                    }               
                    else
                    {
                        tempBrand = col.FindOne(x => x.name.Equals(brandListCombo.Text));
                        if (tempBrand == null)
                        {
                            brand.name = brandListCombo.Text;
                            //brand.brandid = brandIdBox.Text;
                            //brand.folderID = folderIdBox.Text;
                            brand.isChecking = true;
                            col.Insert(brand);
                        }
                        else
                        {
                            tempBrand.name = brandListCombo.Text;
                            //tempBrand.brandid = brandIdBox.Text;
                            //tempBrand.folderID = folderIdBox.Text;
                            tempBrand.isChecking = true;
                            col.Update(tempBrand);
                        }
                        //if (!db.Brands.Contains(brand))
                        //    db.Brands.Add(brand);
                        //else
                        //    db.Brands.Update(brand);
                    }
                    //db.SaveChanges();
                }

                this.DialogResult = true;
            }
            else
            {
                popup1.IsOpen = true;
            }
        }
        private void no_click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
